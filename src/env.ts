/**
 * react-native-config only supports string values.
 * We are using a utility here to parse boolean and number types.
 * We have to make a copy of the values from the Config prior to
 * parsing to prevent an error from trying to parse some extra keys
 * from the Config object prototype chain.
 */
import {
  API_URL,
  DISABLE_WARNINGS,
  PASSWORD_RESET_URL,
  SENTRY_DSN_KEY,
  STORYBOOK_MODE,
  USE_SENTRY,
  WEB_APP_URL,
  FOOTHILL_APPOINTMENT_URL,
  WOMENS_URL,
  TELEHEALTH_URL, //TODO get the correct URL and add to context repo as well
  SELF_ASSESSMENT_URL, //TODO get the correct URL and add to context repo
} from 'react-native-dotenv';

import dotenvParseVariables from 'dotenv-parse-variables';

const ENV = {
  API_URL,
  DISABLE_WARNINGS,
  PASSWORD_RESET_URL,
  SENTRY_DSN_KEY,
  STORYBOOK_MODE,
  USE_SENTRY,
  WEB_APP_URL,
  FOOTHILL_APPOINTMENT_URL,
  WOMENS_URL,
  TELEHEALTH_URL,
  SELF_ASSESSMENT_URL,
};

dotenvParseVariables(ENV);

export default ENV;
