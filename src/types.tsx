import gql from 'graphql-tag';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type AppointmentQueue = {
  __typename?: 'AppointmentQueue';
  queueId: Scalars['ID'];
  queueWaits?: Maybe<WaitInformation>;
};

export type AuthPayload = {
  __typename?: 'AuthPayload';
  token: Scalars['String'];
  user?: Maybe<User>;
};

export type ClassCategory = {
  __typename?: 'ClassCategory';
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  count?: Maybe<Scalars['Int']>;
  thumbnail?: Maybe<Scalars['String']>;
  classGroups?: Maybe<Array<Maybe<ClassGroup>>>;
};

export type ClassGroup = {
  __typename?: 'ClassGroup';
  id: Scalars['ID'];
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  mediaId?: Maybe<Scalars['ID']>;
  media?: Maybe<Media>;
  hoagClassIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  classes?: Maybe<Array<Maybe<HoagClass>>>;
};

export type Device = {
  __typename?: 'Device';
  id: Scalars['ID'];
  FCMToken: Scalars['String'];
  userId?: Maybe<Scalars['Int']>;
};

export type Event = {
  __typename?: 'Event';
  id: Scalars['ID'];
  eventId?: Maybe<Scalars['String']>;
  postId?: Maybe<Scalars['String']>;
  eventSlug?: Maybe<Scalars['String']>;
  eventStatus?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  mediaId?: Maybe<Scalars['Int']>;
  thumbnail?: Maybe<Scalars['String']>;
  date?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  eventStartDate?: Maybe<Scalars['String']>;
  eventEndDate?: Maybe<Scalars['String']>;
  eventStartTime?: Maybe<Scalars['String']>;
  eventEndTime?: Maybe<Scalars['String']>;
  eventStart?: Maybe<Scalars['String']>;
  eventEnd?: Maybe<Scalars['String']>;
  startTime?: Maybe<Scalars['String']>;
  endTime?: Maybe<Scalars['String']>;
  postContent?: Maybe<Scalars['String']>;
  postStatus?: Maybe<Scalars['String']>;
  postDate?: Maybe<Scalars['String']>;
  guid?: Maybe<Scalars['String']>;
  type?: Maybe<ClassGroup>;
  title?: Maybe<Scalars['String']>;
  bookedSpaces?: Maybe<Scalars['Int']>;
  totalSpaces?: Maybe<Scalars['Int']>;
  spacesRemaining?: Maybe<Scalars['Int']>;
  isVirtual?: Maybe<Scalars['Boolean']>;
  youtubeLiveLink?: Maybe<Scalars['String']>;
  class?: Maybe<HoagClass>;
};

export type EventGroup = {
  __typename?: 'EventGroup';
  id: Scalars['ID'];
  eventId?: Maybe<Scalars['String']>;
  postId?: Maybe<Scalars['String']>;
  eventSlug?: Maybe<Scalars['String']>;
  eventStatus?: Maybe<Scalars['Int']>;
  mediaId?: Maybe<Scalars['Int']>;
  thumbnail?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  postContent?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  events?: Maybe<Array<Maybe<Event>>>;
};

export type HoagClass = {
  __typename?: 'HoagClass';
  id: Scalars['ID'];
  date?: Maybe<Scalars['String']>;
  startTime?: Maybe<Scalars['String']>;
  endTime?: Maybe<Scalars['String']>;
  type?: Maybe<ClassGroup>;
  title?: Maybe<Scalars['String']>;
  totalSpaces?: Maybe<Scalars['Int']>;
  spacesRemaining?: Maybe<Scalars['Int']>;
};

export type Hospital = {
  __typename?: 'Hospital';
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['String']>;
  fullAddress?: Maybe<Scalars['String']>;
  phoneInt?: Maybe<Scalars['String']>;
  timeZone?: Maybe<Scalars['String']>;
  todaysBusinessHours?: Maybe<Scalars['String']>;
};

export type HospitalGroup = {
  __typename?: 'HospitalGroup';
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  hospitals: Array<Hospital>;
};

export type Media = {
  __typename?: 'Media';
  id: Scalars['ID'];
  thumbnailImage?: Maybe<Scalars['String']>;
  mediumImage?: Maybe<Scalars['String']>;
  fullImage?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  login?: Maybe<AuthPayload>;
  signup?: Maybe<AuthPayload>;
  createDevice?: Maybe<Device>;
  eventRegistration?: Maybe<Scalars['Boolean']>;
  eventCancelRegistration?: Maybe<Scalars['Boolean']>;
  useToken?: Maybe<Token>;
};

export type MutationLoginArgs = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};

export type MutationSignupArgs = {
  signupPayload?: Maybe<SignupPayload>;
};

export type MutationCreateDeviceArgs = {
  FCMToken?: Maybe<Scalars['String']>;
};

export type MutationEventRegistrationArgs = {
  postId?: Maybe<Scalars['String']>;
};

export type MutationEventCancelRegistrationArgs = {
  postId?: Maybe<Scalars['String']>;
};

export type MutationUseTokenArgs = {
  token?: Maybe<Scalars['String']>;
};

export type Post = {
  __typename?: 'Post';
  id: Scalars['ID'];
  date?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  mediaId?: Maybe<Scalars['ID']>;
  media?: Maybe<Media>;
  isFeatured?: Maybe<Scalars['Boolean']>;
};

export type Query = {
  __typename?: 'Query';
  me?: Maybe<User>;
  device?: Maybe<Device>;
  myEvents?: Maybe<Array<Maybe<Event>>>;
  events?: Maybe<Array<Maybe<Event>>>;
  event?: Maybe<Event>;
  eventGroups?: Maybe<Array<Maybe<EventGroup>>>;
  eventGroup?: Maybe<EventGroup>;
  hospitalGroups: Array<HospitalGroup>;
  hospital: Hospital;
  waitTimes: Array<WaitData>;
  classCategories?: Maybe<Array<Maybe<ClassCategory>>>;
  classCategory?: Maybe<ClassCategory>;
  classGroups?: Maybe<Array<Maybe<ClassGroup>>>;
  classGroup?: Maybe<ClassGroup>;
  classes?: Maybe<Array<Maybe<HoagClass>>>;
  class?: Maybe<HoagClass>;
  biocircuits?: Maybe<Array<Maybe<HoagClass>>>;
  posts?: Maybe<Array<Maybe<Post>>>;
  post?: Maybe<Post>;
  media?: Maybe<Media>;
};

export type QueryEventArgs = {
  eventId?: Maybe<Scalars['Int']>;
};

export type QueryEventGroupsArgs = {
  categoryId?: Maybe<Scalars['Int']>;
};

export type QueryEventGroupArgs = {
  eventGroupId?: Maybe<Scalars['String']>;
};

export type QueryWaitTimesArgs = {
  groupId?: Maybe<Scalars['ID']>;
};

export type QueryClassCategoryArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type QueryClassGroupArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type QueryClassArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type QueryPostArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type QueryMediaArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type SignupPayload = {
  email?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  passwordConfirmation?: Maybe<Scalars['String']>;
};

export type Token = {
  __typename?: 'Token';
  id: Scalars['ID'];
  token: Scalars['String'];
  userId?: Maybe<Scalars['Int']>;
  dateUsed?: Maybe<Scalars['String']>;
  daysValid?: Maybe<Scalars['Int']>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['ID'];
  email?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  tokenExpirationDate?: Maybe<Scalars['String']>;
  device?: Maybe<Device>;
};

export type WaitData = {
  __typename?: 'WaitData';
  hospital?: Maybe<Hospital>;
  waits?: Maybe<WaitInformation>;
  appointmentQueues: Array<AppointmentQueue>;
};

export type WaitInformation = {
  __typename?: 'WaitInformation';
  nextAvailableVisit?: Maybe<Scalars['Int']>;
  currentWait?: Maybe<Scalars['String']>;
  queueLength?: Maybe<Scalars['Int']>;
  queueTotal?: Maybe<Scalars['Int']>;
};
