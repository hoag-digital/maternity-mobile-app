/**
 * This is a MOCK schema for building stateful relations in the UI.
 * This is only a temporary placeholder until a final api-driven schema is established.
 * TODO: Deprecate as actual types are established in the api schema.
 */

export interface UrgentCareFacility {
  /** the name of the appointment location */
  facilityName: string;
  /** uri for the image of the facility */
  facilityAvatarUrl: string;
}
