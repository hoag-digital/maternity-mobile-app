import * as Sentry from '@sentry/react-native';
import ApolloClient from 'apollo-boost';
import React, { Component, ReactNode } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { ApolloProvider } from '@apollo/react-hooks';
import * as RNLocalize from 'react-native-localize';
import RNBootSplash from 'react-native-bootsplash';
import Orientation from 'react-native-orientation-locker';

import { ThemeProvider } from 'emotion-theming';

import Storybook from '../storybook';
import { NotificationsProvider } from './services/notifications';
import { createRootNav } from './navigation/RootNav';
import NavigationService from './navigation/NavigationService';
import { theme } from './styles';
import ENV from './env';
import i18n, { setI18nConfig } from './utils/language';
import AuthProvider from './lib/AuthProvider';
import { AuthUtils, AuthPropType } from './lib/AuthUtils';

const { USE_SENTRY, SENTRY_DSN_KEY, STORYBOOK_MODE, API_URL } = ENV;

if (USE_SENTRY) {
  Sentry.init({
    dsn: SENTRY_DSN_KEY,
  });

  // Setting a custom tag for the api url to help eliminate any confusion when debugging
  Sentry.setTag('apiUrl', API_URL || '');
}

interface State {
  removeWhenYouveAddedStateManagement?: true;
  /** The root store or stores to inject into the app. */
  // store: any; // if you use this, type it to your root store
  /** The configured Apollo client. ONLY USED WITH APOLLO / GRAPHQL APPS */
  // client: ApolloClient<InMemoryCache>;
  /** mock state auth object */
  auth: AuthPropType;
  /** the notifications context type */
  notificationsContext: any;
}

class App extends Component<{}, State> {
  constructor(props) {
    super(props);
    setI18nConfig(); // This needs to be setup immediately or you may receive an error that it can't find the translation

    this.state = {
      auth: {
        token: null,
        saveToken: this.saveToken,
        clearToken: this.clearToken,
      },
      notificationsContext: {
        notificationsShouldBeInitialized: false,
        setNotificationsShouldBeInitialized: this.setNotificationsShouldBeInitialized,
      },
    };

    // Generate the root nav of the app based on presence of a user (restored in setup functions above)
    // Putting this in the constructor so that the navigation doesnt reset when we invoke setState in this component.
    this.navigator = createRootNav({ currentUser: false });
  }

  private saveToken = async (token): Promise<void> => {
    await AuthUtils.saveToken(token);

    this.setState({
      auth: {
        ...this.state.auth,
        token,
      },
    });
  };

  private clearToken = async (): Promise<void> => {
    await AuthUtils.clearToken();

    this.setState({
      auth: {
        ...this.state.auth,
        token: null,
      },
    });
  };

  private setNotificationsShouldBeInitialized = (
    notificationsShouldBeInitialized: boolean
  ): void => {
    this.setState({
      notificationsContext: {
        ...this.state.notificationsContext,
        notificationsShouldBeInitialized,
      },
    });
  };

  private onLocaleChange = (language: string): void => {
    i18n.locale = language;

    this.forceUpdate();
  };

  public componentDidMount(): void {
    /** This locks in portrat mode, as Android fails to listen to manifest on device */
    Orientation.lockToPortrait();
    RNLocalize.addEventListener('change', this.onLocaleChange);

    this.setupApp();
  }

  public componentWillUnmount(): void {
    RNLocalize.removeEventListener('change', this.onLocaleChange);
  }

  private async setupApp(): Promise<void> {
    try {
      const authToken = await AuthUtils.retrieveToken();

      if (authToken) {
        this.setState({
          auth: {
            ...this.state.auth,
            token: authToken,
          },
        });
      }
    } catch (error) {
      Sentry.captureException(error);

      throw error;
    } finally {
      // Now that everything is hydrated, hide the splashscreen
      RNBootSplash.hide({ duration: 250 });
    }
  }

  public render(): ReactNode {
    // uncomment the rest of this method when setting up Apollo or MST/Redux
    const isLoaded = this.state; // && this.state.client or this.state.store

    if (!isLoaded) {
      // render nothing by default, should be covered by the splash screen
      return null;
    }

    const { auth } = this.state;
    const RootNav = this.navigator;

    const client = new ApolloClient({
      name: 'maternity-client',
      uri: `${API_URL}/graphql`,
      request: async (operation): Promise<void> => {
        if (auth && auth.token) {
          operation.setContext({
            headers: {
              authorization: `Bearer ${auth.token}`,
            },
          });
        }
      },
    });

    return (
      <ApolloProvider client={client}>
        <SafeAreaProvider>
          <AuthProvider auth={this.state.auth}>
            <ThemeProvider theme={theme}>
              <NotificationsProvider notificationsContext={this.state.notificationsContext}>
                {STORYBOOK_MODE ? (
                  <Storybook />
                ) : (
                  <RootNav
                    ref={(navigatorRef: any): void => {
                      // setting this reference lets us access navigation actions in app locations
                      // that wouldn't otherwise have access to the navigation prop, such as the
                      // notification event handlers.
                      NavigationService.setTopLevelNavigator(navigatorRef);
                    }}
                  />
                )}
              </NotificationsProvider>
            </ThemeProvider>
          </AuthProvider>
        </SafeAreaProvider>
      </ApolloProvider>
    );
  }
}

// tslint:disable-next-line
export default App;
