import { storiesOf } from '@storybook/react-native';
import React from 'react';
import faker from 'faker';

import { FormFieldError } from './FormFieldError';

storiesOf('components/FormFieldError', module).add('Default', () => (
  <FormFieldError>{faker.lorem.sentence()}</FormFieldError>
));
