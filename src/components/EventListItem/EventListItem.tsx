import React, { FC, ReactElement, useState } from 'react';
import dayjs from 'dayjs';
import { NavigationSwitchScreenProps, withNavigation, NavigationActions } from 'react-navigation';

import { Container } from '../Container';
import { Text } from '../Text';
import { Button } from '../Button';
import { AuthConsumer } from '../../lib/AuthConsumer';
import { colors } from '../../styles';
import { Event } from '../../types';
import CheckIcon from '../../assets/images/icons/check-circle-gray.svg';
import TicketIconWhite from '../../assets/images/icons/ticket-white.svg';
import { SMALLEST_ICON_SIZE, LARGER_ICON_SIZE } from '../../utils/constants';
import { LoginModal } from '../LoginModal';
import getParentRouteFromNavigation from '../../utils/getParentRouteFromNavigation';

export const defaultRegisterButtonProps = {
  borderRadius: 7,
  py: 2,
};

interface Props extends Event {
  /** callback function to intiate the process of reserving a class */
  initiateClassReservation: (classId: string) => void;
  /** whether this event is a program (receives special styles for the ProgramDetailsScreen) */
  isProgram?: boolean;
  /** whether of not the class is full (i.e. can no longer take new reservations) */
  isFull: boolean;
  /** whether the currently authenticated user has already registered for this specific event */
  isRegistered: boolean;
  /** navigation params to be passed to event list item to enable redirect upon login/account creation */
  navigationParams: {
    eventCategoryName: string;
    eventGroupName: string;
    eventCategoryId: string;
  };
}

type ComponentProps = Props & NavigationSwitchScreenProps;

// NOTE: all of these ternary color computations will need to be re-abstracted once Nutrition programs are added!
export const registerButtonBase = (props): React.ReactNode => {
  return (
    <Button
      onPress={props.onReservePress}
      borderColor={props.isProgram ? colors.tangerineYellow : colors.mediumOrchid}
      ignoreDisabledStyles
      {...defaultRegisterButtonProps}
      {...props}
    />
  );
};

export const registerButtonSlotOpen = (props): React.ReactNode =>
  registerButtonBase({
    ...props,
    backgroundColor: props.isProgram ? colors.tangerineYellow : colors.mediumOrchid,
    color: props.isProgram ? colors.black : colors.white,
    label: `${props.spacesRemaining} Left`,
    renderIcon: () => (
      <Container height={LARGER_ICON_SIZE} width={LARGER_ICON_SIZE} pr={3} centerContent>
        <TicketIconWhite
          width={`${SMALLEST_ICON_SIZE}`}
          height={`${SMALLEST_ICON_SIZE}`}
          style={{ marginRight: 10 }}
        />
      </Container>
    ),
  });

export const registerButtonProgram = (props): React.ReactNode => {
  const { isProgram, isFull } = props;

  const myColors = {
    primary: isProgram ? colors.tangerineYellow : colors.mediumOrchid,
    secondary: isProgram ? colors.black : colors.white,
  };

  return registerButtonBase({
    ...props,
    color: isFull ? myColors.primary : myColors.secondary,
    disabled: isFull,
    borderColor: myColors.primary,
    backgroundColor: isFull ? colors.transparent : myColors.primary,
    label: isFull ? 'Class Full' : `${props.spacesRemaining} Left`,
  });
};

export const registerButtonSlotFull = (props): React.ReactNode =>
  registerButtonBase({
    ...props,
    backgroundColor: colors.transparent,
    disabled: true,
    color: props.isProgram ? colors.tangerineYellow : colors.mediumOrchid,
    label: 'Class Full',
  });

export const registerButtonAlreadyRegistered = (props): React.ReactNode =>
  registerButtonBase({
    ...props,
    backgroundColor: colors.transparent,
    color: colors.balticSea,
    disabled: true,
    borderColor: colors.balticSea,
    label: 'Registered',
    renderIcon: () => (
      <Container height={LARGER_ICON_SIZE} width={LARGER_ICON_SIZE} pr={3} centerContent>
        <CheckIcon width="22" height="22" style={{ marginRight: 10 }} />
      </Container>
    ),
  });

export const renderRegisterButton = (props): React.ReactNode => {
  if (props.isRegistered) {
    return registerButtonAlreadyRegistered(props);
  }

  if (props.isProgram) {
    return registerButtonProgram(props);
  }

  if (props.isFull) {
    return registerButtonSlotFull(props);
  }

  return registerButtonSlotOpen(props);
};

/**
 * This component renders an individual wordpress event and its registration status/CTA...
 * as a "Class" on the ClassDetailsScreen,
 * as a "Program" on the ProgramDetailsScreen.
 */
export const EventListItemComponent: FC<ComponentProps> = ({
  postId: classId,
  date,
  isFull,
  isRegistered,
  startTime,
  spacesRemaining,
  endTime,
  initiateClassReservation,
  isProgram = false,
  navigationParams,
  navigation,
}): ReactElement => {
  const formattedDate = date ? dayjs(date).format('dddd, MMMM D') : '';
  const formattedStartTime = startTime ? dayjs(startTime).format('h:mm A') : '';
  const formattedEndTime = endTime ? dayjs(endTime).format('h:mm A') : '';
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const parentRoute = getParentRouteFromNavigation(navigation);

  // Action that is dispatched from the login screen to navigate back to event details.
  // The parentRoute is potentially 'Home', 'ClassCategories', or 'Schedule'
  // however they are guaranteed to already be logged-in on 'Schedule'
  const AUTH_TO_CLASS_DETAILS_REDIRECT_ACTION =
    parentRoute === 'ClassCategories'
      ? NavigationActions.navigate({
          routeName: 'Home',
          action: NavigationActions.navigate({
            routeName: 'ClassCategories',
            action: NavigationActions.navigate({
              routeName: 'ClassGroups',
              params: navigationParams,
            }),
          }),
        })
      : NavigationActions.navigate({
          routeName: 'Home',
        });

  const proceedToClassDetailsScreen = (): void => {
    navigation.navigate({
      routeName: 'ClassDetails',
      params: navigationParams,
    });
  };

  const onCreateAccount = (): void => {
    navigation.navigate('Registration', {
      redirect: {
        action: AUTH_TO_CLASS_DETAILS_REDIRECT_ACTION,
        callback: proceedToClassDetailsScreen,
      },
    });
  };

  const onLogin = (): void => {
    navigation.navigate('Login', {
      redirect: {
        action: AUTH_TO_CLASS_DETAILS_REDIRECT_ACTION,
        callback: proceedToClassDetailsScreen,
      },
    });
  };

  return (
    <AuthConsumer key={classId}>
      {(auth): React.ReactNode => {
        const { token } = auth;

        const isLoggedOut = !token;

        const onReservePress = (): void => {
          if (isLoggedOut) {
            setLoginModalVisible(true);
          } else {
            initiateClassReservation(classId);
          }
        };

        return (
          <>
            <Container fullWidth flexDirection="row" justifyContent="space-between">
              <Container py={2} flexDirection="row" justifyContent="space-between" width="50%">
                <Container flexDirection="row" alignItems="center">
                  <Container>
                    <Text fontSize={2}>{formattedDate}</Text>
                    <Text fontSize={2}>{`${formattedStartTime} - ${formattedEndTime}`}</Text>
                  </Container>
                </Container>
              </Container>
              <Container p={2} width="45%">
                {renderRegisterButton({
                  onReservePress,
                  isFull,
                  isRegistered,
                  isProgram,
                  spacesRemaining,
                })}
              </Container>
            </Container>
            <LoginModal
              isVisible={loginModalVisible}
              setIsVisible={setLoginModalVisible}
              createAccount={onCreateAccount}
              login={onLogin}
            />
          </>
        );
      }}
    </AuthConsumer>
  );
};

export const EventListItem = withNavigation(EventListItemComponent);
