/**
 * This component was copied from
 * https://github.com/torgeadelin/react-native-animated-nav-tab-bar
 * to override hard-coded styles
 *
 * TODO: re-implement styles with @emotion/native and deprecate styled-components from project
 */
import React, { Component, ReactElement } from 'react';
import styled, { css } from 'styled-components/native';
import { Animated, View } from 'react-native';
import PropTypes from 'prop-types';

import { AuthConsumer } from '../../lib/AuthConsumer';
import { t } from '../../utils';
import { space } from '../../styles/margins';
import { colors } from '../../styles/colors';
import { isIphoneX } from './utils/iPhoneX';

// These are the routes which should only be accessible when logged-in
// const AUTH_PROTECTED_ROUTES = ['Programs', 'Schedule'];

//Wrapper
const BOTTOM_PADDING = '10px';
const BOTTOM_PADDING_IPHONE_X = '30px';

/**
 * Returns an i18n route name for the route of the tab navigator.
 * "routeName" params map to the keys of the navigationOptions object.
 */
export function getLabelForRoute(routeName: string): string {
  let label = '';

  switch (routeName) {
    case 'Home':
      label = t('navigation.home');
      break;
    case 'ClassCategories':
      label = t('navigation.classes');
      break;
    case 'Me':
      label = t('navigation.me');
      break;
    case 'Schedule':
      label = t('navigation.schedule');
      break;
    default:
      break;
  }

  return label;
}

// Shadow
const SHADOW = css`
  shadow-color: #000000;
  shadow-offset: 0px 5px;
  shadow-opacity: 0.05;
  elevation: 1;
  shadow-radius: 20px;
`;

// NOTE: The absolute positioning of this component is what required this component be copied from
// its source repository. The absolute positioning is required for scrollviews to descend below the
// semi-transparent background of the tab bar.
const Wrapper = styled.View`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  flex-direction: row;
  width: 100%;
  elevation: 2;
  padding-bottom: ${isIphoneX() ? BOTTOM_PADDING_IPHONE_X : BOTTOM_PADDING};
  padding-top: ${(props): string => `${props.topPadding}px`};
  padding-horizontal: ${(props): string => `${props.verticalPadding}px`};
  background-color: ${(props): any => props.tabBarBackground};
  ${(props): any => props.shadow && SHADOW};
`;

const TabButton = styled.TouchableOpacity`
  flex: 1;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-radius: 100px;
  padding-vertical: 10px;
  flex-grow: ${(props): number => (props.isRouteActive ? props.labelLength / 10 + 1 : 1)};
`;

// NOTE: This has a "fontWeight: bold;" style which also had to be manually removed.
const Label = styled(Animated.Text)`
  color: ${(props): any => props.activeColor};
  margin-left: ${(props): string => (props.icon ? '8px' : '0px')};
`;

const Dot = styled(Animated.View)`
  position: absolute;
  top: ${(props): string => `${props.topPadding}px`};
  width: ${(props): string => `${props.width}px`};
  height: ${(props): string => `${props.height}px`};
  border-radius: 100px;
  background-color: ${(props): any => props.activeTabBackground};
  z-index: -1;
`;

export class AnimatedTabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prevPos: this.props.verticalPadding,
      pos: 0,
      width: 0,
      height: 0,
      animatedPos: new Animated.Value(1),
    };
  }

  animation = (value): any =>
    Animated.spring(value, {
      toValue: 1,
    });

  componentDidMount(): void {
    this.animation(this.state.animatedPos).start(() => {
      this.setState({
        prevPos: this.props.verticalPadding,
      });
    });
  }

  componentDidUpdate(prevProps): void {
    if (prevProps.navigation.state.index !== this.props.navigation.state.index) {
      this.animation(this.state.animatedPos).start(() => {
        this.setState({
          prevPos: this.state.pos,
        });
      });
    }
  }

  render(): ReactElement {
    const {
      activeColors,
      activeTabBackgrounds,
      getAccessibilityLabel,
      inactiveTintColor,
      navigation,
      onTabLongPress,
      onTabPress,
      renderIcon,
      shadow,
      tabBarBackground,
      topPadding,
      verticalPadding,
    } = this.props;

    const { routes, index: activeRouteIndex } = navigation.state;

    const activeTabBackground = activeTabBackgrounds
      ? Array.isArray(activeTabBackgrounds)
        ? activeTabBackgrounds[activeRouteIndex] || '#E4F7F7'
        : activeTabBackgrounds
      : '#E4F7F7';

    const activeColor = activeColors
      ? Array.isArray(activeColors)
        ? activeColors[activeRouteIndex] || '#000'
        : activeColors
      : '#000';

    return (
      <AuthConsumer>
        {(): ReactElement => {
          return (
            <Wrapper
              topPadding={topPadding}
              verticalPadding={verticalPadding}
              tabBarBackground={tabBarBackground}
              shadow={shadow}
            >
              {routes.map((route, routeIndex) => {
                const isRouteActive = routeIndex === activeRouteIndex;
                const tintColor = isRouteActive ? activeColor : inactiveTintColor;
                const labelText = getLabelForRoute(route.routeName);
                const labelLength = labelText.length;
                const tabHasIcon = renderIcon({ route, focused: isRouteActive, tintColor }) != null;

                // Render Label if tab is selected or if there is no icon
                const renderLabel = (): ReactElement => {
                  const label = (
                    <Label icon={tabHasIcon} activeColor={activeColor}>
                      {labelText}
                    </Label>
                  );

                  if (isRouteActive) {
                    return label;
                  } else if (!isRouteActive && !tabHasIcon) {
                    return label;
                  } else {
                    return;
                  }
                };

                return (
                  <TabButton
                    icon={tabHasIcon}
                    labelLength={labelLength}
                    onLayout={(event): void => {
                      isRouteActive &&
                        this.setState({
                          pos: event.nativeEvent.layout.x,
                          width: event.nativeEvent.layout.width,
                          height: event.nativeEvent.layout.height,
                        });
                    }}
                    isRouteActive={isRouteActive}
                    key={routeIndex}
                    onPress={(): void => {
                      if (!isRouteActive) {
                        /**
                         * this logic, in addition to the AuthConsumer above, is a custom addition that
                         * is not part of the core AnimatedTabBar component we copied
                         * NOTE: disabling tab-bar direct auth protection but leaving in place for future use
                         */
                        // if (isLoggedOut && AUTH_PROTECTED_ROUTES.includes(route.routeName)) {
                        //   if (setLoginModalVisible) {
                        //     setLoginModalVisible(true);
                        //   }

                        //   return;
                        // }

                        this.animation(this.state.animatedPos).start(() => {
                          this.setState({
                            prevPos: this.state.pos,
                          });

                          this.state.animatedPos.setValue(0);
                        });

                        onTabPress({ route });
                      }
                    }}
                    onLongPress={(): void => {
                      if (!isRouteActive) {
                        this.animation(this.state.animatedPos).start(() => {
                          this.setState({
                            prevPos: this.state.pos,
                          });

                          this.state.animatedPos.setValue(0);
                        });

                        onTabLongPress({ route });
                      }
                    }}
                    accessibilityLabel={getAccessibilityLabel({ route })}
                  >
                    <View>{renderIcon({ route, focused: isRouteActive, tintColor })}</View>
                    {renderLabel()}
                  </TabButton>
                );
              })}
              <Dot
                topPadding={topPadding}
                activeTabBackground={activeTabBackground}
                style={{
                  left: this.state.animatedPos.interpolate({
                    inputRange: [0, 1],
                    outputRange: [this.state.prevPos, this.state.pos],
                  }),
                }}
                width={this.state.width}
                height={this.state.height}
              />
            </Wrapper>
          );
        }}
      </AuthConsumer>
    );
  }
}

AnimatedTabBar.propTypes = {
  activeTabBackgrounds: PropTypes.string.isRequired,
  tabBarBackground: PropTypes.string.isRequired,
  shadow: PropTypes.bool.isRequired,
  verticalPadding: PropTypes.number.isRequired,
  topPadding: PropTypes.number.isRequired,
};

AnimatedTabBar.defaultProps = {
  activeTabBackgrounds: colors.blueTransparent,
  tabBarBackground: colors.whiteTransparent,
  shadow: true,
  verticalPadding: space[2],
  topPadding: space[2],
};
