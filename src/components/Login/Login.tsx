import React, { useState, FC, createRef } from 'react';
import { BorderProps, ColorProps, SpaceProps, FlexProps } from 'styled-system';
import { Icon } from 'react-native-elements';
import { Button } from '../Button';
import { Text } from '../Text';
import { TextInput } from '../TextInput';
import { Touchable } from '../Touchable';
import { Container } from '../Container';

import { colors } from '../../styles';
import { t } from '../../utils';

interface LoginProps {
  loginLoading: boolean;
  loginError: boolean;
  /** the callbacks to be invoked onPress */
  loginPress: (email: string, password: string) => void;
  registrationPress: () => void;
  forgotPasswordPress: () => void;
}

type ComponentProps = LoginProps & FlexProps & SpaceProps & BorderProps & ColorProps;

export const Login: FC<ComponentProps> = ({
  loginLoading,
  loginError,
  loginPress,
  registrationPress,
  forgotPasswordPress,
  ...props
}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const passwordRef = createRef();

  return (
    <Container
      bg={colors.snow}
      borderRadius={20}
      elevation={4}
      overflow="visible"
      padding={4}
      shadowColor={colors.silver}
      shadowOffset={{ width: 0, height: 0 }}
      shadowOpacity={0.15}
      shadowRadius={5}
      {...props}
    >
      <Text fontSize={32} fontWeight="bold" marginBottom="10%" marginTop="10%">
        {/* might need to figure out the new line part in translate  */}
        {/* Welcome, please {'\n'}sign in. */}
        {t('login.welcome')}
      </Text>
      <TextInput
        onChangeText={(value: string): void => setEmail(value)}
        onSubmitEditing={(): void => (passwordRef as any).current.focus()}
        value={email}
        keyboardType="email-address"
        placeholder={t('login.email')}
        icon={<Icon name="envelope" type="font-awesome" color={colors.whisper} />}
        marginTop={2}
        borderRadius={5}
        returnKeyType="next"
        borderColor={loginError ? colors.cinnabar : colors.whisper}
      />
      <TextInput
        ref={passwordRef}
        onChangeText={(value: string): void => setPassword(value)}
        onSubmitEditing={(): void => loginPress(email, password)}
        value={password}
        placeholder={t('login.password')}
        icon={<Icon name="key" type="font-awesome" color={colors.whisper} />}
        secureTextEntry
        marginTop={2}
        borderRadius={5}
        returnKeyType="send"
        borderColor={loginError ? colors.cinnabar : colors.whisper}
      />
      <Touchable onPress={(): void => forgotPasswordPress()} fullWidth alignItems="flex-end">
        <Text fontSize={1} marginTop={2} color={colors.primary}>
          {t('login.buttons.forgotPassword')}
        </Text>
      </Touchable>
      <Button
        disabled={!email || !password}
        loading={loginLoading}
        width="100%"
        marginTop={40}
        label={t('login.buttons.login')}
        color={colors.white}
        py={3}
        onPress={(): void => loginPress(email, password)}
      />
      <Text
        fontSize={2}
        fontWeight="bold"
        color={loginError ? 'red' : colors.transparent}
        textAlign="center"
      >
        {t('login.error')}
      </Text>

      <Container flexDirection="row" marginTop={20}>
        <Container top={-9} flex={1} borderBottomWidth={1} borderBottomColor={colors.silver} />
        <Text color={colors.silver}>&nbsp; or &nbsp;</Text>
        <Container top={-9} flex={1} borderBottomWidth={1} borderBottomColor={colors.silver} />
      </Container>
      <Button
        py={3}
        width="100%"
        marginTop={20}
        label={t('login.buttons.createAccount')}
        backgroundColor={colors.white}
        color={colors.silver}
        borderColor={colors.silver}
        elevation={6}
        borderWidth={1}
        onPress={(): void => registrationPress()}
      />
    </Container>
  );
};
