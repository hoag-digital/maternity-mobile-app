import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { TransparencyImageOverlay } from './TransparencyImageOverlay';

storiesOf('components/TransparencyImageOverlay', module).add('Default', () => (
  <TransparencyImageOverlay height={400} />
));
