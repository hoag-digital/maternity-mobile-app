import React, { FC } from 'react';
import { Icon } from 'react-native-elements';
import CalendarPicker from 'react-native-calendar-picker';
import dayjs from 'dayjs';

import { colors, space } from '../../styles';
import { Container } from '../Container';
import { Modal } from '../Modal';
import { Text } from '../Text';
import { Touchable } from '../Touchable';

interface Props {
  /** whether the modal is visible */
  isVisible: boolean;
  /** callback function to toggle the modal visibility */
  setIsVisible: (value: boolean) => void;
  /** Text to display in title bar; defaults to 'Select Date' */
  title?: string;
  /** Default date to display, must allow undefined to satisfy options from Datepicker */
  defaultDate?: Date | undefined;
  /** Value to set selected date in screen implementation */
  setSelectedDate: (value: Date) => void;
  /**value of selected date from implementation */
  selectedDate?: Date | undefined;
  /**allow range selection of dates */
  allowRange?: boolean;
}

/**
 * Component docs: Date picker icon that will bring a modal up from bottom of the screen with a datepicker
 */
export const Datepicker: FC<Props> = ({
  isVisible,
  setIsVisible,
  title = 'Calendar',
  defaultDate,
  selectedDate,
  setSelectedDate,
  allowRange = false,
}) => {
  const startRangeStyle = {
    backgroundColor: colors.black,

    borderRadius: 25,
    height: 30,
    width: 30,
  };

  const endRangeStyle = {
    backgroundColor: colors.terra,
    borderRadius: 25,
    height: 30,
    width: 30,
  };

  const selectedStyle = {
    backgroundColor: colors.wewak,
    color: colors.white,
    height: 40,
    width: 40,
  };

  const dateElementStyle = {
    backgroundColor: colors.white,
    marginLeft: 5,
    alignItems: 'center',
    alignContent: 'center',
    borderRadius: 35,
    width: 75,
    height: 50,
    paddingTop: 3,
  };

  const linkStyle = { paddingRight: space[3] };

  return (
    <>
      <Touchable
        onPress={(): void => {
          setIsVisible(true);
        }}
      >
        <Container flexDirection="row" alignItems="center">
          <Icon name="today" raised color={colors.chino} />
          <Container {...dateElementStyle}>
            <Text fontSize={4} color={colors.gray}>
              {selectedDate ? dayjs(selectedDate).format('MMM') : 'Month'}
            </Text>
          </Container>
          <Container {...dateElementStyle}>
            <Text fontSize={4} color={colors.gray}>
              {selectedDate ? dayjs(selectedDate).format('DD') : 'Day'}
            </Text>
          </Container>
          <Container {...dateElementStyle}>
            <Text fontSize={4} color={colors.gray}>
              {selectedDate ? dayjs(selectedDate).format('YYYY') : 'Year'}
            </Text>
          </Container>
        </Container>
      </Touchable>
      <Modal isVisible={isVisible} setIsVisible={setIsVisible} bottomHalf displayClose={false}>
        <Container fullWidth centerContent>
          <Text>{title}</Text>
          <CalendarPicker
            allowRangeSelection={allowRange}
            todayBackgroundColor={colors.transparent}
            todayTextStyle={{ color: colors.black }}
            selectedDayTextColor={colors.white}
            selectedStartDate={defaultDate}
            selectedRangeStartStyle={startRangeStyle}
            selectedRangeEndStyle={endRangeStyle}
            selectedRangeStyle={selectedStyle}
            selectedDayStyle={selectedStyle}
            onDateChange={(date, type): void => {
              setSelectedDate(date);
            }}
          />
          <Container flexDirection="row" fullWidth justifyContent="flex-end" mb={20}>
            <Touchable onPress={(): void => setIsVisible(false)} {...linkStyle}>
              <Text>Cancel</Text>
            </Touchable>
            <Touchable onPress={(): void => setIsVisible(false)} {...linkStyle}>
              <Text>Ok</Text>
            </Touchable>
          </Container>
        </Container>
      </Modal>
    </>
  );
};

export default Datepicker;
