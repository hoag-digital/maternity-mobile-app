import React, { ReactElement } from 'react';
import { storiesOf } from '@storybook/react-native';
import { Text } from '../Text';
import { Button } from '../Button';
import { colors } from '../../styles/colors';
import { Modal } from './Modal';

const ModalTest = (): ReactElement => {
  const [isVisible, setIsVisible] = React.useState(false);
  return (
    <>
      <Button
        width="100%"
        marginTop={2}
        marginLeft={2}
        marginright={2}
        label="Show Modal"
        color={colors.white}
        borderRadius={5}
        onPress={(): void => {
          setIsVisible(true);
        }}
      />
      <Modal isVisible={isVisible} setIsVisible={setIsVisible}>
        <>
          <Text>You: "Hello modal content!"</Text>
          <Text>Modal: "Hello developer"</Text>
        </>
      </Modal>
    </>
  );
};

const BottomModalTest = (): ReactElement => {
  const [isVisible, setIsVisible] = React.useState(false);
  return (
    <>
      <Button
        width="100%"
        marginTop={2}
        marginLeft={2}
        marginright={2}
        label="Show Modal"
        color={colors.white}
        borderRadius={5}
        onPress={(): void => {
          setIsVisible(true);
        }}
      />
      <Modal isVisible={isVisible} setIsVisible={setIsVisible} bottomHalf>
        <>
          <Text>You: "Hello modal content!"</Text>
          <Text paddingBottom={50}>Modal: "Hello developer"</Text>
        </>
      </Modal>
    </>
  );
};

storiesOf('components/Modal', module)
  .add('Default modal', () => <ModalTest key="default" />)
  .add('Bottom Half Modal', () => <BottomModalTest key="bottom" />);
