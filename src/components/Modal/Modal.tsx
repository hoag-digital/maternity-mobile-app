import React, { FC, ReactElement } from 'react';
import RNModal from 'react-native-modal';
import { View, ViewStyle } from 'react-native';
import { Icon } from 'react-native-elements';

import { Container } from '../Container';
import { Touchable } from '../Touchable';
import { colors } from '../../styles/colors';
import { LARGER_ICON_SIZE, DEFAULT_HITSLOP } from '../../utils';

interface Props {
  children: ReactElement;
  /* Whether or not the Modal is visible */
  isVisible: boolean;
  /* The function for setting the visibility */
  setIsVisible: (visible: boolean) => void;
  /* Whether or not the Modal should render from the bottom portion of the screen, peeking up */
  bottomHalf?: boolean;
  /** display the close button, defaults to true */
  displayClose?: boolean;
}

/**
 * Displays a basic Modal
 */
export const Modal: FC<Props> = ({
  children,
  isVisible,
  setIsVisible,
  bottomHalf = false,
  displayClose = true,
}) => {
  const handleCloseModal = (): void => setIsVisible(false);
  const borderTopRadius = 20;
  const borderBottomRadius = bottomHalf ? 0 : borderTopRadius;

  const bottomHalfStyle: ViewStyle = {
    justifyContent: 'flex-end',
    margin: 0,
  };

  return (
    <View>
      <RNModal
        isVisible={isVisible}
        onBackButtonPress={(): void => handleCloseModal()}
        onBackdropPress={(): void => handleCloseModal()}
        useNativeDriver // Makes the animation smoother
        hideModalContentWhileAnimating // Recommended when useNativeDriver is set to trues
        style={bottomHalf ? bottomHalfStyle : null}
      >
        <Container
          bg={colors.snow}
          borderTopLeftRadius={borderTopRadius}
          borderTopRightRadius={borderTopRadius}
          borderBottomLeftRadius={borderBottomRadius}
          borderBottomRightRadius={borderBottomRadius}
          elevation={4}
          overflow="visible"
          padding={4}
          shadowColor={colors.silver}
          shadowOffset={{ width: 0, height: 0 }}
          shadowOpacity={0.15}
          shadowRadius={5}
        >
          {displayClose ? (
            <Touchable
              position="absolute"
              top={2}
              right={5}
              onPress={(): void => handleCloseModal()}
              hitSlop={DEFAULT_HITSLOP}
            >
              <Icon name="ios-close" type="ionicon" size={LARGER_ICON_SIZE} color={colors.black} />
            </Touchable>
          ) : null}
          {children}
        </Container>
      </RNModal>
    </View>
  );
};

export default Modal;
