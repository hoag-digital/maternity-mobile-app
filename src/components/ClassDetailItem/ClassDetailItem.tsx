import React, { FC, ReactElement } from 'react';
import { Icon } from 'react-native-elements';

import { Container } from '../Container';
import { Text } from '../../components/Text';
import { colors } from '../../styles';
import { SMALLEST_ICON_SIZE } from '../../utils';

interface Props {
  /** the name of the icon from the icon set */
  iconName: string;
  /** the name of the icon set from react-native-vector-icons */
  iconType: string;
  /** what kind of information the label signifies */
  label: string;
  /** the actual label value to display */
  value: string;
}

/**
 * This component is used to display a specific piece of information on the class details screen
 */
export const ClassDetailItem: FC<Props> = ({ iconType, iconName, label, value }): ReactElement => {
  return (
    <>
      <Container flexDirection="row">
        <Container>
          <Icon
            size={SMALLEST_ICON_SIZE}
            name={iconName}
            type={iconType}
            color={colors.balticSea}
          />
        </Container>
        <Container fill>
          <Text px={2} fontSize={2} color={colors.balticSea50}>
            {label}
          </Text>
        </Container>
      </Container>
      <Container flexDirection="row">
        <Container width={SMALLEST_ICON_SIZE} />
        <Container fill>
          <Text px={2} fontSize={2}>
            {value}
          </Text>
        </Container>
      </Container>
    </>
  );
};

export default ClassDetailItem;
