import React, { FC, ReactNode } from 'react';
import FastImage from 'react-native-fast-image';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import {
  CARD_BORDER_RADIUS,
  DEFAULT_CONTENT_DESCRIPTION_LENGTH,
  TOUCHABLE_PRESS_DELAY,
} from '../../utils';
import { trimText } from '../../utils/trimText';
import { colors } from '../../styles';
import { space, radii } from '../../styles/margins';
import NewsIcon from '../../assets/images/icons/news.svg';
import ClassIcon from '../../assets/images/icons/classes-label.svg';
import { EventGroup } from '../../types';
import GenericClassImage from '../../assets/images/generic_class.jpg';

interface CardProps {
  /** Callback function to invoke when the root container is pressable. Renders a Touchable when not null */
  onPress?: () => void;
  /** composition function to render the top portion of the Card */
  renderTop: () => ReactNode;
  /** composition function to render the bottom segment of the Card */
  renderBottom: () => ReactNode;
  /** overridable props for wrapper <Container> / <Touchable>. Will be merged with defaults. */
  containerProps?: object;
}

const defaultContainerProps = {
  my: 1,
  minHeight: 150, // default is the smallest of all card variants
  borderRadius: CARD_BORDER_RADIUS,
  borderWidth: 1,
  borderColor: colors.whisper,
  bg: colors.white,
  centerContent: true,
  elevation: 4,
  shadowColor: colors.snow,
  shadowOffset: { width: 5, height: 0 },
  shadowOpacity: 0.9,
  shadowRadius: 5,
};

// TODO: go through file and extract common values into constants
/**
 * This is the core of the compositional Card component leveraged by most of the other Card-style
 * components of the app.
 */
export const Card: FC<CardProps> = ({ onPress, renderTop, renderBottom, containerProps }) => {
  const mergedContainerProps = {
    ...defaultContainerProps,
    ...containerProps,
  };

  /**
   * Note: delayPressIn is an undocumented TouchableOpacity prop that delays the opacity overlay from
   * triggering. This is added here since the Card component is typically rendered in a listview which
   * the user can scroll. This is an attempt to remove the opacity from the "scroll gesture" but to
   * leave it in place for the "card press gesture".
   */
  return onPress ? (
    <Touchable onPress={onPress} delayPressIn={TOUCHABLE_PRESS_DELAY} {...mergedContainerProps}>
      {renderTop()}
      {renderBottom()}
    </Touchable>
  ) : (
    <Container {...mergedContainerProps}>
      {renderTop()}
      {renderBottom()}
    </Container>
  );
};

interface ContentCardProps {
  /** Link to remote image used as background cover on card (preferred) */
  imageSrc?: string | null | undefined;
  /** Local image asset used as background cover on card (fallback) */
  imageAsset?: number;
  /** Article heading (title) */
  heading: string | null | undefined;
  /** Description text displayed under heading */
  text: string | null | undefined;
  /** Description of cover image (used for accessibility label) */
  label?: string;
  /** What label should be displayed on card */
  cardLabel?: string;
  /** What color should the container be for the card label */
  cardLabelBackgroundColor?: string;
  /** Callback function to invoke on press */
  onPress?: () => void;
}

/**
 * This is the image/description combination card for news, classes, and videos displayed on the
 * Home Screen.
 */
export const ContentCard: FC<ContentCardProps> = ({
  onPress,
  heading,
  imageSrc,
  imageAsset,
  label,
  cardLabel,
  cardLabelBackgroundColor,
  text,
}) => {
  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <Container
            fullWidth
            borderTopLeftRadius={CARD_BORDER_RADIUS}
            borderTopRightRadius={CARD_BORDER_RADIUS}
            position="relative"
          >
            <FastImage
              accessibilityLabel={label}
              style={{
                width: '100%',
                height: 150,
                borderTopLeftRadius: CARD_BORDER_RADIUS,
                borderTopRightRadius: CARD_BORDER_RADIUS,
              }}
              source={imageSrc ? { uri: imageSrc } : imageAsset}
              resizeMode="cover"
            />
          </Container>
        );
      }}
      renderBottom={(): React.ReactNode => {
        return (
          <>
            <Container
              backgroundColor={cardLabelBackgroundColor}
              borderRadius={radii[1]}
              px={2}
              py={1}
              position="absolute"
              top={10}
              left={10}
              flexDirection="row"
              justifyContent="center"
              alignItems="center"
            >
              <NewsIcon height={10} width={10} />
              <Text fontSize={1} color={colors.white} marginLeft={1}>
                {cardLabel}
              </Text>
            </Container>

            <Container px={4} py={5} justifyContent="space-around" fullWidth fill>
              <Text fontWeight="bold" fontSize={3}>
                {heading}
              </Text>
              <Text fontSize={2} pt={1}>
                {text ? trimText(text, DEFAULT_CONTENT_DESCRIPTION_LENGTH) : null}
              </Text>
            </Container>
          </>
        );
      }}
    />
  );
};

interface ClassCategoryCardProps {
  /** Link to remote image used as background cover on card (preferred) */
  imageSrc?: string | null | undefined;
  /** Local image asset used as background cover on card (fallback) */
  imageAsset?: number;
  /** Label for the cover image */
  label?: string;
  /** Callback function to invoke on press */
  onPress?: () => void;
}

/**
 * CategoryCard is a pressable card with a full image overlay and a class category label.
 */
export const ClassCategoryCard: FC<ClassCategoryCardProps> = props => {
  const { label, imageSrc, imageAsset, onPress } = props;

  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <>
            <FastImage
              accessibilityLabel={`This is an image card for ${label}`}
              style={{
                width: '100%',
                height: 170,
                borderRadius: CARD_BORDER_RADIUS,
              }}
              source={imageSrc ? { uri: imageSrc } : imageAsset}
              resizeMode="cover"
            />
            <Container position="absolute" bottom={CARD_BORDER_RADIUS} left={0} flexDirection="row">
              <Container px={2} bg="transparent">
                <Text
                  textDecorationLine="none"
                  color={colors.white}
                  fontWeight="bold"
                  fontSize={4}
                  textAlign="left"
                >
                  {label}
                </Text>
              </Container>
            </Container>
          </>
        );
      }}
      renderBottom={(): ReactNode => {
        return null;
      }}
    />
  );
};

interface ProgramCardProps {
  /** Whether to highlight the card label */
  highlighted?: boolean;
  /** Link to remote image used as background cover on card (preferred) */
  imageSrc?: string;
  /** Local image asset used as background cover on card (fallback) */
  imageAsset?: number;
  /** Label for the cover image */
  label?: string;
  /** Callback function to invoke on press */
  onPress?: () => void;
}

/**
 * ProgramCard is a pressable card with a full image overlay and a program label.
 */
export const ProgramCard: FC<ProgramCardProps> = ({
  label,
  imageAsset,
  imageSrc,
  onPress,
  highlighted,
}) => {
  const fontSize = 30;
  const labelHeight = fontSize + 2 * space[1];

  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <>
            <FastImage
              accessibilityLabel={`This is an image card for ${label}`}
              style={{
                width: '100%',
                height: 250,
                borderRadius: CARD_BORDER_RADIUS,
              }}
              source={imageSrc ? { uri: imageSrc } : imageAsset}
              resizeMode="cover"
            />
            <Container
              position="absolute"
              zIndex={20}
              bottom={CARD_BORDER_RADIUS}
              left={0}
              flexDirection="row"
            >
              <Container px={2} bg={highlighted ? colors.tangerineYellow : 'transparent'}>
                <Text
                  textDecorationLine="none"
                  color={highlighted ? 'black' : 'white'}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={fontSize}
                  lineHeight={labelHeight}
                >
                  {label}
                </Text>
              </Container>
              {highlighted ? (
                <Container
                  width={0}
                  height={0}
                  bg="transparent"
                  borderStyle="solid"
                  borderRightWidth={labelHeight / 2}
                  borderTopWidth={labelHeight}
                  borderRightColor="transparent"
                  borderTopColor={colors.tangerineYellow}
                />
              ) : null}
            </Container>
            <Container
              position="absolute"
              bottom={CARD_BORDER_RADIUS}
              left={150}
              flexDirection="row"
            >
              <Container px={2} bg={highlighted ? colors.eclipse : 'transparent'}>
                <Text
                  textDecorationLine="none"
                  color={highlighted ? 'white' : 'black'}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={fontSize - 15}
                  marginLeft={15}
                  marginRight={15}
                  lineHeight={labelHeight}
                >
                  Learn More
                </Text>
              </Container>
              {highlighted ? (
                <Container
                  width={0}
                  height={0}
                  bg="transparent"
                  borderStyle="solid"
                  borderRightColor="transparent"
                  borderTopColor={colors.eclipse}
                />
              ) : null}
            </Container>
          </>
        );
      }}
      renderBottom={(): ReactNode => {
        return null;
      }}
    />
  );
};

interface EventGroupCardProps extends EventGroup {
  /** Callback function to invoke on press */
  onPress?: () => void;
  /** Determines if we display a featured label on the card */
  featured?: boolean;
}

/**
 * This is the list item to be displayed on the EventGroupsScreen
 */
export const EventGroupCard: FC<EventGroupCardProps> = ({
  name,
  thumbnail,
  description,
  onPress,
  featured = false,
}) => {
  const trimmedDescription = description
    ? trimText(description, DEFAULT_CONTENT_DESCRIPTION_LENGTH)
    : null;

  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <Container
            fullWidth
            borderTopLeftRadius={CARD_BORDER_RADIUS}
            borderTopRightRadius={CARD_BORDER_RADIUS}
            position="relative"
          >
            <FastImage
              accessibilityLabel={`${name} class card`}
              style={{
                width: '100%',
                height: 200,
                resizeMode: 'cover',
                borderTopLeftRadius: CARD_BORDER_RADIUS,
                borderTopRightRadius: CARD_BORDER_RADIUS,
              }}
              source={thumbnail ? { uri: thumbnail } : GenericClassImage}
            />
          </Container>
        );
      }}
      renderBottom={(): React.ReactNode => {
        return (
          <>
            {featured ? (
              <Container
                backgroundColor={colors.mediumOrchid}
                borderRadius={radii[1]}
                px={2}
                py={1}
                position="absolute"
                top={10}
                left={10}
                flexDirection="row"
                justifyContent="center"
                alignItems="center"
              >
                <ClassIcon height={10} width={10} />
                <Text fontSize={1} color={colors.white} marginLeft={1}>
                  Live Stream
                </Text>
              </Container>
            ) : null}
            <Container padding={4} justifyContent="space-around" fullWidth fill>
              <Container pb={2} flexDirection="row" justifyContent="space-between">
                <Container width="80%">
                  <Text fontWeight="bold">{name}</Text>
                </Container>
              </Container>
              <Text fontSize={2}>{trimmedDescription}</Text>
            </Container>
          </>
        );
      }}
    />
  );
};

export default Card;
