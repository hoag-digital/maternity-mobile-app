import React, { FC } from 'react';
import { Icon } from 'react-native-elements';

import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { Modal } from '../Modal';
import { Container } from '../Container';
import AllIcon from '../../assets/images/filter/all.svg';
import ClassIcon from '../../assets/images/filter/classes.svg';
import NewsIcon from '../../assets/images/filter/news.svg';
import VideoIcon from '../../assets/images/filter/videos.svg';

import { colors, fontSizes, space } from '../../styles';
import { translate } from '../../utils';

/**
 * Component docs: Displays a Filter Icon that will bring up the bottom modal
 */
export const Filter: FC = () => {
  const [isVisible, setIsVisible] = React.useState(false);

  const rowStyle = {
    flexDirection: 'column',
    alignItems: 'center',
    paddingVertical: space[1],
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: colors.whisper,
    width: '85%',
  };

  const itemStyle = {
    flexDirection: 'row',
    width: '50%',
    paddingLeft: '10%',
    alignItems: 'center',
    alignContent: 'center',
  };

  const iconStyle = { height: 15, width: 15 };
  const textStyle = { paddingLeft: space[1], paddingRight: space[1], fontSize: fontSizes[3] };

  return (
    <>
      <Touchable
        onPress={(): void => {
          setIsVisible(true);
        }}
      >
        <Container flexDirection="row" alignItems="center">
          <Icon name="filter-list" />
          <Text fontSize={1}>{translate('filter.label')}</Text>
        </Container>
      </Touchable>
      <Modal isVisible={isVisible} setIsVisible={setIsVisible} bottomHalf>
        <Container fullWidth centerContent>
          <Container
            borderBottomWidth={3}
            borderBottomColor={colors.silver}
            width="20%"
            alignContent="center"
            marginBottom={space[2]}
          />
          <Container {...rowStyle}>
            <Container {...itemStyle}>
              <AllIcon {...iconStyle} />
              <Text {...textStyle}>{translate('filter.all')}</Text>
            </Container>
          </Container>
          <Container {...rowStyle}>
            <Container {...itemStyle}>
              <NewsIcon {...iconStyle} />
              <Text {...textStyle}>{translate('filter.news')}</Text>
              <Icon name="check" color={colors.terra} />
            </Container>
          </Container>
          <Container {...rowStyle}>
            <Container {...itemStyle}>
              <ClassIcon {...iconStyle} />
              <Text {...textStyle}>{translate('filter.classes')}</Text>
            </Container>
          </Container>
          <Container {...rowStyle}>
            <Container {...itemStyle}>
              <VideoIcon {...iconStyle} />
              <Text {...textStyle}>{translate('filter.videos')}</Text>
              <Icon name="check" color={colors.terra} />
            </Container>
          </Container>
        </Container>
      </Modal>
    </>
  );
};

export default Filter;
