import React, { FC } from 'react';

import { Container } from '../Container';
import { Text } from '../Text';
import { colors } from '../../styles';
import LabelIcon from '../../assets/images/icons/classes-label.svg';

interface Props {
  /** prop info */
  cool: boolean;
}

const DEFAULT_PROPS: Partial<Props> = {
  cool: true,
};

/**
 * Component docs: what does this component do?
 */
export const ClassesCategoryLabel: FC<Props> = (props = DEFAULT_PROPS) => {
  return (
    <Container
      zIndex={51}
      backgroundColor={colors.mediumOrchid}
      borderTopRightRadius={2}
      borderBottomRightRadius={2}
      flexDirection="row"
      overflow="hidden"
      p={1}
      pr={2}
      pl={4}
      {...props}
    >
      <LabelIcon width={15} height={15} marginRight={5} />
      <Text color={colors.white} fontSize={2} fontWeight="700">
        Classes
      </Text>
    </Container>
  );
};

export default ClassesCategoryLabel;
