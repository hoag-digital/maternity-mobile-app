import React, { useRef, useState, FC } from 'react';
import SignatureCapture from 'react-native-signature-capture';
import dayjs from 'dayjs';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';
import { t } from '../../utils';

interface Props {
  /** The user name to display on the signature form */
  name: string;
  /** Callback function to invoke when submitting the signature form */
  onSubmit: (string) => void;
}

/**
 * This allows us to capture a User's signature for the class waiver
 */
export const SignatureForm: FC<Props> = props => {
  const { name, onSubmit } = props;
  const [submitEnabled, setSubmitEnabled] = useState(false);
  const signatureInputRef: any = useRef(null);
  const date = dayjs().format('M/D/YYYY');

  const onCreateSignature = (): void => {
    setSubmitEnabled(true);
  };

  const clearSignature = (): void => {
    if (signatureInputRef.current) {
      signatureInputRef.current.resetImage();
    }

    if (submitEnabled) {
      setSubmitEnabled(false);
    }
  };

  const submitSignatureForm = (): void => {
    if (signatureInputRef.current) {
      signatureInputRef.current.saveImage();
    }
  };

  const submitSignature = (result: any): void => {
    onSubmit(result.encoded);
  };

  return (
    <Container
      alignItems="center"
      position="relative"
      bottom={0}
      bg={colors.white}
      height={200}
      width="100%"
      py={3}
      px={30}
    >
      <Container width="100%" borderBottomWidth={1} borderBottomColor={colors.solitude}>
        <SignatureCapture
          ref={signatureInputRef}
          onSaveEvent={submitSignature}
          style={{ height: 100, width: '100%' }}
          onDragEvent={onCreateSignature}
          saveImageFileInExtStorage={false}
          showBorder={false}
          showNativeButtons={false}
          showTitleLabel={false}
          viewMode="portrait"
        />
      </Container>
      <Container width="100%" flexDirection="row" justifyContent="space-between">
        <Container p={3} width="33%" alignItems="flex-start">
          <Text fontSize={1}>{name}</Text>
        </Container>
        <Container p={3} width="33%" centerContent>
          <Text fontSize={1}>{`${t('classes.waiver.date')}: ${date}`}</Text>
        </Container>
        <Container p={3} width="33%" alignItems="flex-end">
          <Touchable onPress={clearSignature}>
            <Text fontSize={1} color={colors.cinnabar}>
              {t('classes.waiver.clear')}
            </Text>
          </Touchable>
        </Container>
      </Container>
      <Touchable
        width="100%"
        bg={submitEnabled ? colors.aqua : colors.eclipse}
        borderRadius={5}
        onPress={submitSignatureForm}
        centerContent
      >
        <Text py={2} color={submitEnabled ? colors.white : colors.gray}>
          {t('classes.waiver.agree')}
        </Text>
      </Touchable>
    </Container>
  );
};

export default SignatureForm;
