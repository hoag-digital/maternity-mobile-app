import React, { FC } from 'react';
import { colors } from '../../styles';

import { Container } from '../Container';
import { Text } from '../Text';

interface Props {
  children: React.ReactNode;
}

/**
 * UI for general error messaging that appears above a form.
 */
export const FormErrorSummary: FC<Props> = props => {
  const { children } = props;

  return (
    <Container borderColor={colors.terra} borderRadius={2} borderWidth={2} p={4}>
      <Text>{children}</Text>
    </Container>
  );
};

export default FormErrorSummary;
