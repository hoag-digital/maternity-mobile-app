import React, { ReactNode, FC } from 'react';
import { StatusBar } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { FlexProps, SpaceProps } from 'styled-system';

import { colors } from '../../styles';
import { ScreenHeader } from '../ScreenHeader';
import { ModalHeader } from '../ModalHeader';
import { Container } from '../Container';

interface ScreenProps {
  /* whether to display the screen header component (logo and hamburger menu) */
  screenHeader?: boolean;
  /* whether to display the modal header component (screen title and back button) */
  modalHeader?: boolean;
  /** The screen title to display; no title will be shown unless explicitly provided; only for ModalHeader */
  screenTitle?: string;
  /** Custom background color of the screen */
  backgroundColor?: string;
  /** Custom background color for the modalHeader component */
  headerColor?: string;
  /** Custom font color for the modalHeader component */
  headerFontColor?: string;
  /* whether to override all background styles for a transparent background */
  transparent?: boolean;
  /* content to render on the right side of the modalHeader */
  rightHeaderContent?: ReactNode;
  /** the content to render within the screen */
  children?: ReactNode;
}

type ComponentProps = ScreenProps & FlexProps & SpaceProps;

export const Screen: FC<ComponentProps> = ({
  backgroundColor,
  headerColor,
  headerFontColor,
  children,
  screenTitle,
  screenHeader,
  modalHeader,
  transparent,
  rightHeaderContent,
  ...screenProps
}) => {
  const hasTopInset = screenHeader || modalHeader;

  // the root container should be the header color by default to handle overflowing
  let rootContainerBackgroundColor = headerColor;

  // override the root container color if using the "screenHeader"
  if (screenHeader) {
    rootContainerBackgroundColor = colors.chino;
  }

  // override the root container color if specifying "transparent"
  if (transparent) {
    rootContainerBackgroundColor = colors.transparent;
  }

  return (
    <Container fill fullWidth backgroundColor={rootContainerBackgroundColor}>
      <StatusBar
        translucent
        animated
        backgroundColor={colors.transparent}
        barStyle="dark-content"
      />
      <SafeAreaView
        style={{ flex: 1 }}
        forceInset={{ top: hasTopInset ? 'always' : 'never', bottom: 'never' }}
      >
        {screenHeader ? <ScreenHeader drawerEnabled /> : null}
        {modalHeader ? (
          <ModalHeader
            backEnabled
            screenTitle={screenTitle}
            backgroundColor={headerColor}
            fontColor={headerFontColor}
            rightHeaderContent={rightHeaderContent}
          />
        ) : null}
        <Container bg={transparent ? colors.transparent : backgroundColor} {...screenProps}>
          {children}
        </Container>
      </SafeAreaView>
    </Container>
  );
};

Screen.defaultProps = {
  headerColor: colors.white,
  screenHeader: false,
  screenTitle: null,
  modalHeader: false,
};
