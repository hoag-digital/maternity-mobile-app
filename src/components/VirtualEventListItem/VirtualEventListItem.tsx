import React, { FC, ReactElement, useState } from 'react';
import dayjs from 'dayjs';
import { NavigationSwitchScreenProps, withNavigation, NavigationActions } from 'react-navigation';
import { Icon } from 'react-native-elements';

import { Container } from '../Container';
import { Text } from '../Text';
import { Button } from '../Button';
import { AuthConsumer } from '../../lib/AuthConsumer';
import { colors } from '../../styles';
import { Event } from '../../types';
import TicketIconWhite from '../../assets/images/icons/ticket-white.svg';
import { LoginModal } from '../LoginModal';
import { SMALLEST_ICON_SIZE, LARGER_ICON_SIZE } from '../../utils/constants';
import { safelyOpenUrl } from '../../utils/safelyOpenUrl';
import getParentRouteFromNavigation from '../../utils/getParentRouteFromNavigation';

export const defaultRegisterButtonProps = {
  borderRadius: 7,
  py: 2,
};

interface Props extends Event {
  /** callback function to intiate the process of reserving a class */
  initiateClassReservation: (classId?: string | null) => void;
  /** whether this event is a program (receives special styles for the ProgramDetailsScreen) */
  isProgram?: boolean;
  /** whether of not the class is full (i.e. can no longer take new reservations) */
  isFull: boolean;
  /** whether the currently authenticated user has already registered for this specific event */
  isRegistered: boolean;
  /** navigation params to be passed to event list item to enable redirect upon login/account creation */
  navigationParams: {
    eventCategoryName: string;
    eventGroupName: string;
    eventCategoryId: string;
  };
}

type ComponentProps = Props & NavigationSwitchScreenProps;

// NOTE: all of these ternary color computations will need to be re-abstracted once Nutrition programs are added!
export const registerButtonBase = (props): React.ReactNode => {
  return (
    <Button
      onPress={props.onReservePress}
      ignoreDisabledStyles
      {...defaultRegisterButtonProps}
      {...props}
    />
  );
};

/** This should render a button which allows the user to register */
export const registerButtonUnregistered = (props): React.ReactNode =>
  registerButtonBase({
    ...props,
    backgroundColor: colors.mediumOrchid,
    color: colors.white,
    label: 'Reserve',
    renderIcon: () => (
      <Container height={LARGER_ICON_SIZE} width={LARGER_ICON_SIZE} pr={3} centerContent>
        <TicketIconWhite
          width={`${SMALLEST_ICON_SIZE}`}
          height={`${SMALLEST_ICON_SIZE}`}
          style={{ marginRight: 10 }}
        />
      </Container>
    ),
  });

/** This should render a button which allows the user to navigate to the virtual event link */
export const registerButtonRegistered = (props): React.ReactNode =>
  registerButtonBase({
    ...props,
    backgroundColor: colors.transparent,
    color: colors.mediumOrchid,
    borderColor: colors.mediumOrchid,
    label: 'Live Stream',
    renderIcon: () => {
      return (
        <Container pr={3}>
          <Icon
            size={LARGER_ICON_SIZE}
            name="youtube"
            type="material-community"
            color={colors.mediumOrchid}
          />
        </Container>
      );
    },
  });

export const renderRegisterButton = (props): React.ReactNode => {
  return props.isRegistered ? registerButtonRegistered(props) : registerButtonUnregistered(props);
};

/**
 * This component is very similar to the EventListItem component except it facilitates linking to a
 * virtual class event link.
 */
export const VirtualEventListItemComponent: FC<ComponentProps> = ({
  postId: classId,
  date,
  isFull,
  isRegistered,
  startTime,
  spacesRemaining,
  endTime,
  initiateClassReservation,
  isProgram = false,
  navigationParams,
  navigation,
  youtubeLiveLink,
}): ReactElement => {
  const formattedDate = date ? dayjs(date).format('dddd, MMMM D') : '';
  const formattedStartTime = startTime ? dayjs(startTime).format('h:mm A') : '';
  const formattedEndTime = endTime ? dayjs(endTime).format('h:mm A') : '';
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const parentRoute = getParentRouteFromNavigation(navigation);

  // Action that is dispatched from the login screen to navigate back to event details.
  // The parentRoute is potentially 'Home', 'ClassCategories', or 'Schedule'
  // however they are guaranteed to already be logged-in on 'Schedule'
  const AUTH_TO_CLASS_DETAILS_REDIRECT_ACTION =
    parentRoute === 'ClassCategories'
      ? NavigationActions.navigate({
          routeName: 'Home',
          action: NavigationActions.navigate({
            routeName: 'ClassCategories',
            action: NavigationActions.navigate({
              routeName: 'ClassGroups',
              params: navigationParams,
            }),
          }),
        })
      : NavigationActions.navigate({
          routeName: 'Home',
        });

  const proceedToClassDetailsScreen = (): void => {
    navigation.navigate({
      routeName: 'ClassDetails',
      params: navigationParams,
    });
  };

  const onCreateAccount = (): void => {
    navigation.navigate('Registration', {
      redirect: {
        action: AUTH_TO_CLASS_DETAILS_REDIRECT_ACTION,
        callback: proceedToClassDetailsScreen,
      },
    });
  };

  const onLogin = (): void => {
    navigation.navigate('Login', {
      redirect: {
        action: AUTH_TO_CLASS_DETAILS_REDIRECT_ACTION,
        callback: proceedToClassDetailsScreen,
      },
    });
  };

  return (
    <AuthConsumer key={classId}>
      {(auth): React.ReactNode => {
        const { token } = auth;
        const isLoggedOut = !token;

        const onReservePress = (): void => {
          if (isLoggedOut) {
            setLoginModalVisible(true);
          } else if (isRegistered) {
            safelyOpenUrl(youtubeLiveLink);
          } else {
            initiateClassReservation(classId);
          }
        };

        return (
          <>
            <Container fullWidth flexDirection="row" justifyContent="space-between">
              <Container py={2} flexDirection="row" justifyContent="space-between" width="50%">
                <Container flexDirection="row" alignItems="center">
                  <Container>
                    <Text fontSize={2}>{formattedDate}</Text>
                    <Text fontSize={2}>{`${formattedStartTime} - ${formattedEndTime}`}</Text>
                  </Container>
                </Container>
              </Container>
              <Container p={2} width="45%">
                {renderRegisterButton({
                  onReservePress,
                  isFull,
                  isRegistered,
                  isProgram,
                  spacesRemaining,
                })}
              </Container>
            </Container>
            <LoginModal
              isVisible={loginModalVisible}
              setIsVisible={setLoginModalVisible}
              createAccount={onCreateAccount}
              login={onLogin}
            />
          </>
        );
      }}
    </AuthConsumer>
  );
};

export const VirtualEventListItem = withNavigation(VirtualEventListItemComponent);
