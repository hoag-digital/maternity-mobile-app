import React, { FC } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { NavigationSwitchScreenProps, withNavigation } from 'react-navigation';
import { FlexProps, SpaceProps } from 'styled-system';

import { Icon, Avatar } from 'react-native-elements';

import Logo from '../../assets/images/hoag-logo-white.svg';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';
import {
  LARGER_ICON_SIZE,
  STATUS_BAR_PADDING_SCALE,
  t,
  DEFAULT_HITSLOP,
  DEFAULT_AVATAR_SIZE,
  DEFAULT_ICON_SIZE,
  LARGEST_ICON_SIZE,
} from '../../utils';
import { LoggedOutContent } from '../../lib/AuthUtils';
import MeSvg from '../../assets/images/icons/mom.svg';

interface ScreenHeaderProps extends NavigationSwitchScreenProps {
  /** Whether to display the drawer menu button */
  drawerEnabled?: boolean;
}

type ComponentProps = ScreenHeaderProps & FlexProps & SpaceProps;

const ScreenHeaderComponent: FC<ComponentProps> = ({ drawerEnabled, navigation }) => {
  const toggleDrawer = (): void => {
    navigation.toggleDrawer();
  };

  const navigateToLogin = (): void => {
    navigation.navigate('Login');
  };

  return (
    <Container bg={colors.chino} minHeight={50} flexDirection="row" pt={STATUS_BAR_PADDING_SCALE}>
      <Container
        minHeight={30}
        flexDirection="row"
        justifyContent="space-around"
        alignItems="center"
        fullWidth
        borderBottomWidth={1}
        borderBottomColor={colors.gray}
        pt={STATUS_BAR_PADDING_SCALE}
        pb={2}
      >
        <Container width="30%" flexDirection="row" justifyContent="flex-start">
          {drawerEnabled && (
            <Touchable testID="drawer-icon" onPress={toggleDrawer} px={3} hitSlop={DEFAULT_HITSLOP}>
              <Icon name="ios-menu" type="ionicon" size={LARGER_ICON_SIZE} color={colors.white} />
            </Touchable>
          )}
        </Container>
        <Container width="40%" centerContent flexDirection="row">
          <Logo height="35" width={75} />
          <Text color={colors.white} fontSize={3} marginTop={2}>
            {` | ${t('home.name')}`}
          </Text>
        </Container>
        <Container width="30%" alignItems="flex-end" pr={3}>
          {/* TODO: add navigation to a user profile screen and figure out the default icon to display */}
          <Avatar
            rounded
            showEditButton
            icon={{
              name: 'user',
              type: 'font-awesome',
              size: DEFAULT_ICON_SIZE,
              color: colors.aqua,
            }}
          />
        </Container>
      </Container>
      {/* Global login banner for all screens leveraging this component */}
      <LoggedOutContent>
        <TouchableWithoutFeedback onPress={navigateToLogin}>
          <Container py={1} centerContent bg={colors.wewak}>
            {/* TODO: i18n */}
            <Text fontSize={1} color={colors.white}>
              Login to gain full access to the app!{' '}
              <Text fontWeight="bold" fontSize={1} color={colors.white}>
                →
              </Text>
            </Text>
          </Container>
        </TouchableWithoutFeedback>
      </LoggedOutContent>
      {/* end Login banner */}
    </Container>
  );
};

ScreenHeaderComponent.defaultProps = {
  drawerEnabled: false,
};

export const ScreenHeader = withNavigation(ScreenHeaderComponent);
