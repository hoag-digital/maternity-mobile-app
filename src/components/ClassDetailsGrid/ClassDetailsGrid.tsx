import React, { FC, ReactElement } from 'react';

import { Container } from '../Container';
import { ClassDetailItem } from '../ClassDetailItem';

import { HoagClass } from '../../types';

/**
 * This component renders a grid layout of the details of a ClassGroup on the ClassDetailsScreen
 * NOTE: this component lives at the ClassGroup level but relies on HoagClass data... design error
 * TODO: potentially deprecate based on design revisions....
 */
export const ClassDetailsGrid: FC<HoagClass> = ({
  spacesRemaining,
  totalSpaces,
}: HoagClass): ReactElement => {
  return (
    <>
      <Container flexDirection="row">
        <Container width="50%" py={2}>
          <ClassDetailItem
            iconName="clock"
            iconType="material-community"
            label="Duration"
            value="1 hour"
          />
        </Container>
        <Container width="50%" py={2}>
          <ClassDetailItem
            iconName="ticket"
            iconType="entypo"
            label="Seats"
            value={`${spacesRemaining} spaces left`}
          />
        </Container>
      </Container>
      <Container flexDirection="row">
        <Container width="50%" py={2}>
          <ClassDetailItem
            iconName="account-group"
            iconType="material-community"
            label="Group Size"
            value={`Up to ${totalSpaces} people`}
          />
        </Container>
        <Container width="50%" py={2}>
          <ClassDetailItem
            iconName="food-apple"
            iconType="material-community"
            label="Includes"
            value="Snacks & Water"
          />
        </Container>
      </Container>
    </>
  );
};

export default ClassDetailsGrid;
