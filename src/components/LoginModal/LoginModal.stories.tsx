import { storiesOf } from '@storybook/react-native';
import React, { ReactElement } from 'react';

import { colors } from '../../styles';
import { Button } from '../Button';
import { LoginModal } from './LoginModal';

const ModalTest = (): ReactElement => {
  const [isVisible, setIsVisible] = React.useState(false);
  return (
    <>
      <Button
        width="100%"
        marginTop={2}
        marginLeft={2}
        marginright={2}
        label="Show Login Modal"
        color={colors.white}
        backgroundColor={colors.aqua}
        borderRadius={5}
        onPress={(): void => {
          setIsVisible(true);
        }}
      />
      <LoginModal
        isVisible={isVisible}
        setIsVisible={setIsVisible}
        login={(): void => console.log('nav to login')}
        createAccount={(): void => console.log('nav to registration')}
      >
        <></>
      </LoginModal>
    </>
  );
};

storiesOf('components/LoginModal', module).add('Default', () => <ModalTest />);
