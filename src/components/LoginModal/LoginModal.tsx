import React, { FC } from 'react';

import { Button } from '../Button';
import { Modal } from '../Modal';
import { Text } from '../Text';
import { colors } from '../../styles';
import { Touchable } from '../Touchable';
import { Container } from '../Container';

interface Props {
  /* Whether or not the Modal is visible */
  isVisible: boolean;
  /* The function for setting the visibility */
  setIsVisible: (visible: boolean) => void;
  /* The function that will be used when clicking create account */
  createAccount: () => void;
  /* The function that will be used when clicking login */
  login: () => void;
}

/**
 * Component docs: Component of type Modal that displays to prompt user to login when attempting perform certain actions or view some content
 */
export const LoginModal: FC<Props> = ({ isVisible, setIsVisible, createAccount, login }) => {
  return (
    <Modal isVisible={isVisible} setIsVisible={setIsVisible} displayClose={false}>
      <Container mt={2} mb={1}>
        <Text fontSize={5} my={2}>
          Please login to continue.
        </Text>
        <Button
          py={3}
          label="Login"
          color={colors.white}
          backgroundColor={colors.wewak}
          borderRadius={40}
          fullWidth
          mt={5}
          onPress={(): void => {
            setIsVisible(false);
            login();
          }}
        />

        <Touchable
          onPress={(): void => {
            setIsVisible(false);
            createAccount();
          }}
          alignItems="center"
          my={5}
        >
          <Text color={colors.aqua} fontSize={1}>
            Create Account
          </Text>
        </Touchable>
      </Container>
    </Modal>
  );
};

export default LoginModal;
