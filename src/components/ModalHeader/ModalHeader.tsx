import React, { FC, ReactNode } from 'react';
import { FlexProps, SpaceProps } from 'styled-system';
import { NavigationSwitchScreenProps, withNavigation } from 'react-navigation';
import { Icon } from 'react-native-elements';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';
import { LARGEST_ICON_SIZE, STATUS_BAR_PADDING_SCALE, DEFAULT_HITSLOP } from '../../utils';

interface ModalHeaderProps extends NavigationSwitchScreenProps {
  /** The custom function to override the back navigation */
  backAction?: () => void;
  /** Whether to display the back button */
  backEnabled?: boolean;
  /** The screen title to display; no title will be shown unless explicitly provided */
  screenTitle?: string;
  /** Whether to use an X icon instead of the typical "back" icon */
  useCloseIcon?: boolean;
  /** Custom background color of the modal header component */
  backgroundColor?: string;
  /** Custom font color of the modal header component */
  fontColor?: string;
  /* content to render on the right side of the modalHeader */
  rightHeaderContent?: ReactNode;
  /** Optional subtitle */
  subtitle?: string;
}

type ComponentProps = ModalHeaderProps & FlexProps & SpaceProps;

const ModalHeaderComponent: FC<ComponentProps> = ({
  backAction,
  backEnabled,
  backgroundColor,
  fontColor,
  navigation,
  screenTitle,
  rightHeaderContent,
}) => {
  const goBack = (): void => {
    if (backAction) {
      backAction();
    } else {
      navigation.goBack();
    }
  };

  return (
    <Container
      bg={backgroundColor}
      minHeight={50}
      flexDirection="row"
      fullWidth
      pt={STATUS_BAR_PADDING_SCALE}
    >
      <Container
        flexDirection="row"
        width="20%"
        px={5}
        alignItems="center"
        justifyContent="flex-start"
      >
        {backEnabled && (
          <Touchable testID="back-icon" onPress={goBack} hitSlop={DEFAULT_HITSLOP}>
            <Icon
              name="ios-arrow-round-back"
              type="ionicon"
              size={LARGEST_ICON_SIZE}
              color={fontColor}
            />
          </Touchable>
        )}
      </Container>

      <Container centerContent flexDirection="row" fill pb={3}>
        {screenTitle ? (
          <Text color={fontColor} fontSize={5} marginTop={2} fontWeight="bold" textAlign="center">
            {screenTitle}
          </Text>
        ) : null}
      </Container>

      <Container width="20%" px={5} centerContent>
        {rightHeaderContent ? rightHeaderContent : null}
      </Container>
    </Container>
  );
};

ModalHeaderComponent.defaultProps = {
  backgroundColor: colors.transparent,
  fontColor: colors.black,
  backEnabled: false,
  screenTitle: null,
  drawerEnabled: false,
};

export const ModalHeader = withNavigation(ModalHeaderComponent);
