import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import React, { FC, ReactElement } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { ActivityIndicator, RefreshControl } from 'react-native';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

import { EventGroupCard } from '../../components/Card';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import { EventGroup } from '../../types';
import { Text } from '../../components/Text';

export const EVENT_GROUPS_FOR_EVENT_CATEGORY_QUERY = gql`
  query eventGroups($eventCategoryId: Int) {
    eventGroups(categoryId: $eventCategoryId) {
      name
      description
      thumbnail
    }
  }
`;

/**
 * This screen displays groupings of upcoming wordpress events that have been grouped by common titles
 * and description data. The events are filtered by the event-category passed as a navigation param
 * from the preceding screen. Selecting a group takes the user to a screen showing an expended description
 * and the individual timeslots for the event. Classes are referred to as "events" in the wordpress api,
 * and subsequently our graphql api, but are displayed to the user under the name "Class".
 */
export const ClassGroupsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const eventCategoryId = navigation.getParam('eventCategoryId');
  const eventCategoryName = navigation.getParam('eventCategoryName');

  const {
    error: eventGroupsError,
    data: eventGroupsData = {},
    loading: eventGroupsLoading,
    refetch: refetchEventGroups,
  } = useQuery(EVENT_GROUPS_FOR_EVENT_CATEGORY_QUERY, {
    fetchPolicy: 'cache-and-network',
    variables: {
      eventCategoryId: parseInt(eventCategoryId, 10), // typing mismatch from wordpress-json to mysql migration
    },
  });

  const navigateToClassDetailsScreen = (eventGroupName?: string | null): void => {
    // NOTES:
    // 1. we are currently using the title of an eventGroup as its id
    // 2. we are passing all accumulated navigation params so that we can recreate the navigation
    // ... history when redirected to and from login/account creation screens
    if (eventGroupName) {
      navigation.navigate('ClassDetails', {
        eventGroupName,
        eventCategoryName,
        eventCategoryId,
      });
    }
  };

  const renderEventGroup = (eventGroup: EventGroup): ReactElement => {
    return (
      <Container key={eventGroup.name} pb={2}>
        <EventGroupCard
          {...eventGroup}
          onPress={(): void => navigateToClassDetailsScreen(eventGroup.name)}
        />
      </Container>
    );
  };

  const hasNoValidEventGroups =
    !eventGroupsData || !eventGroupsData.eventGroups || !eventGroupsData.eventGroups.length;

  const renderEventGroups = (): ReactElement[] => {
    if (hasNoValidEventGroups) return [];

    return eventGroupsData.eventGroups.map(renderEventGroup);
  };

  const renderEventGroupsScreenContent = (): ReactElement => {
    if (eventGroupsLoading && !Object.keys(eventGroupsData).length) {
      return (
        <Container fill pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          <Container fill fullWidth centerContent>
            <ActivityIndicator />
          </Container>
        </Container>
      );
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */

    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        refreshControl={
          <RefreshControl refreshing={eventGroupsLoading} onRefresh={refetchEventGroups} />
        }
        contentContainerStyle={{ flexGrow: 1 }}
      >
        {hasNoValidEventGroups ? (
          <Container fill fullWidth centerContent pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} px={5}>
            <Text textAlign="center" fontSize={3} color={colors.eclipse} pb={1}>
              There are no upcoming {eventCategoryName} classes.
            </Text>
          </Container>
        ) : (
          <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
            {renderEventGroups()}
          </Container>
        )}
      </ScrollView>
    );
  };

  if (eventGroupsError) {
    // TODO: handle graphql errors
  }

  return (
    <Screen
      testID="class-groups-screen"
      modalHeader
      screenTitle={eventCategoryName}
      backgroundColor={colors.white}
      headerColor={colors.aqua}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {renderEventGroupsScreenContent()}
    </Screen>
  );
};
