import React, { FC, ReactElement } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

import { Divider } from 'react-native-elements';
import { ProgramCard } from '../../components/Card';
import { Container } from '../../components/Container';
import { Text } from '../../components/Text';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t, SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import Biocircuit from '../../assets/images/biocircuit.jpg';

// NOTE: commenting out all data fetching until "Biocircuit - Program" data is api-driven
// TODO: "programs" endpoint which returns both Biocircuit as well as Nutrition data

export const ProgramListScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const navigateToProgramIntro = (): void => {
    navigation.navigate('ProgramIntro');
  };

  const renderProgramListScreenContent = (): ReactElement => {
    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <ScrollView scrollIndicatorInsets={{ right: 1 }}>
        <Container mx={5}>
          <Text testID="program-screen-header" fontWeight="bold" fontSize={5} p={4}>
            {t('programs.categories.header')}
          </Text>
          <Divider style={{ backgroundColor: colors.silver }} />
        </Container>
        <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          <ProgramCard
            // TODO: Update to come from API with image when it is available
            // {...classProperties}
            // id={id}
            label="Biocircuit"
            highlighted
            imageAsset={Biocircuit}
            onPress={(): void => navigateToProgramIntro()}
          />
        </Container>
      </ScrollView>
    );
  };

  return (
    <Screen
      testID="program-list-screen"
      screenHeader
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
    >
      {renderProgramListScreenContent()}
    </Screen>
  );
};
