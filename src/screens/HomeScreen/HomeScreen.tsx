import React, { FC, ReactElement } from 'react';
import { ActivityIndicator, RefreshControl } from 'react-native';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

import { EventGroupCard, ContentCard } from '../../components/Card';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';

import { Post, EventGroup } from '../../types';
import { colors } from '../../styles';
import { t, SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import GenericNewsImage from '../../assets/images/generic_news.jpg';

export const HOME_SCREEN_QUERY = gql`
  query content {
    posts(displayAll: false) {
      id
      title
      date
      description
      media {
        fullImage
      }
      mediaId
      isFeatured
    }
    featuredPosts {
      id
      title
      date
      description
      media {
        fullImage
      }
      mediaId
      isFeatured
    }
    featuredEventGroups {
      name
      description
      thumbnail
    }
  }
`;

export const HomeScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const navigateToPostDetailsScreen = (post: Post): void => {
    navigation.navigate('PostDetails', { postId: post.id });
  };

  const navigateToClassDetailsScreen = (eventGroupName?: string | null): void => {
    // NOTE/TODO: We need to update the navigation logic in case the user logs in from the class details
    // screen nested in the Home tab... (i.e. redirect needs to take them to the same tab)
    // leaving old nav params for now to further indicate how this is different from the class stack
    if (eventGroupName) {
      navigation.navigate('ClassDetails', {
        eventGroupName,
        // eventCategoryName,
        // eventCategoryId,
      });
    }
  };

  const {
    error: homeScreenContentError,
    data: homeScreenContentData = {},
    loading: homeScreenContentLoading,
    refetch: refetchHomeScreenContent,
  } = useQuery(HOME_SCREEN_QUERY, {
    fetchPolicy: 'cache-and-network',
  });

  const renderPost = (post: Post): ReactElement => {
    const { id, title, description, media, isFeatured } = post;

    return (
      <Container key={id} pb={2}>
        <ContentCard
          id={id}
          heading={title}
          text={description}
          label=""
          cardLabel={isFeatured ? 'Featured News' : 'News'}
          cardLabelBackgroundColor={isFeatured ? colors.sunsetOrange : colors.summerSky}
          imageSrc={media?.fullImage ? media.fullImage : null}
          imageAsset={GenericNewsImage}
          onPress={(): void => navigateToPostDetailsScreen(post)}
        />
      </Container>
    );
  };

  const renderPosts = (posts?: Post[]): ReactElement | null => {
    if (!posts || !posts.length) return null;

    return <>{posts.map(renderPost)}</>;
  };

  const renderEventGroup = (eventGroup: EventGroup): ReactElement => {
    return (
      <Container key={eventGroup.name} pb={2}>
        <EventGroupCard
          {...eventGroup}
          featured
          onPress={(): void => navigateToClassDetailsScreen(eventGroup.name)}
        />
      </Container>
    );
  };

  const renderEventGroups = (eventGroups?: EventGroup[]): ReactElement | null => {
    if (!eventGroups || !eventGroups.length) return null;

    return <>{eventGroups.map(renderEventGroup)}</>;
  };

  const renderPostsAndEvents = (): ReactElement => {
    const content = homeScreenContentData || {};
    const { featuredEventGroups, featuredPosts, posts } = content;

    return (
      <Container px={5}>
        {renderPosts(featuredPosts)}
        {renderEventGroups(featuredEventGroups)}
        {renderPosts(posts)}
      </Container>
    );
  };

  const renderHomeScreenContent = (): ReactElement => {
    if (homeScreenContentLoading && !Object.keys(homeScreenContentData).length) {
      return (
        <Container fill pt={3} pb={SCROLLVIEW_BOTTOM_PADDING}>
          <Container fullWidth centerContent pt={2} pb={3}>
            <Text testID="hoag-mantra" fontWeight="bold" fontSize={5}>
              {t('home.mantra')}
            </Text>
          </Container>
          <Container fill fullWidth centerContent>
            <ActivityIndicator />
          </Container>
        </Container>
      );
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={homeScreenContentLoading}
            onRefresh={refetchHomeScreenContent}
          />
        }
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pt={3} pb={SCROLLVIEW_BOTTOM_PADDING}>
          <Container fullWidth centerContent pt={2} pb={3}>
            <Text testID="hoag-mantra" fontWeight="bold" fontSize={5}>
              {t('home.mantra')}
            </Text>
          </Container>
          {renderPostsAndEvents()}
        </Container>
      </ScrollView>
    );
  };

  if (homeScreenContentError) {
    // TODO: handle graphql errors
  }

  return (
    <Screen
      testID="home-screen"
      screenHeader
      backgroundColor={colors.pearlLusta}
      flex={1}
      height="100%"
      paddingTop={0}
    >
      {renderHomeScreenContent()}
    </Screen>
  );
};
