import React, { FC, useEffect } from 'react';
import styled from '@emotion/native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import * as Animatable from 'react-native-animatable';
import { ImageBackground, StatusBar, StyleSheet } from 'react-native';
import Logo from '../../../assets/images/hoag-logo-white.svg';
import backgroundImg from '../../../assets/images/pink-background.png';
import { Screen } from '../../../components/Screen';
import { Text } from '../../../components/Text';
import { Container } from '../../../components/Container';
import { t } from '../../../utils';
import { colors, space } from '../../../styles';
import { setOnboarded } from '../../../utils/Onboarding';

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

export const CustomizingExperienceScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  useEffect(() => {
    setTimeout(async () => {
      setOnboarded();
      navigation.navigate('Home');
    }, 4000);
  });

  return (
    <BackgroundImage source={backgroundImg} resizeMode="stretch">
      <Screen testID="customizeExperienceScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <Container justifyContent="center" alignItems="center" pt={100}>
          <Logo height="100" width="100" />
          <Animatable.View
            animation="fadeOutUp"
            easing="ease-in-out"
            duration={1000}
            delay={500}
            style={{ paddingTop: space[7], position: 'absolute', top: 200 }}
          >
            <Text color={colors.white} fontSize={6} textAlign="center">
              {t('onboarding.customization.line1')}
            </Text>
          </Animatable.View>
          <Animatable.View
            animation="fadeInUp"
            duration={3000}
            delay={1000}
            style={{ paddingTop: space[7], position: 'absolute', top: 200 }}
          >
            <Text color={colors.white} fontSize={6} textAlign="center">
              {t('onboarding.customization.line2')}
            </Text>
          </Animatable.View>
        </Container>
      </Screen>
    </BackgroundImage>
  );
};
