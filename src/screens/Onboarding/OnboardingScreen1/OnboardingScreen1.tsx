import React, { FC, ReactNode, useState } from 'react';
import { StatusBar } from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { Screen } from '../../../components/Screen';
import { Text } from '../../../components/Text';
import { colors, fonts, fontSizes, space } from '../../../styles';
import { Container } from '../../../components/Container';
import { SCREEN_HEIGHT, SCREEN_WIDTH, t } from '../../../utils';
import { setUserStatus, USER_ROLE } from '../../../utils/Onboarding';
import { Button } from '../../../components/Button';

const checkboxStyle = {
  iconRight: true,
  right: true,
  checkedColor: colors.oxley,
  containerStyle: { backgroundColor: colors.transparent, borderWidth: 0 },
  textStyle: { color: colors.gray, fontSize: 24, fontWeight: '400', paddingRight: 75 },
  checkedIcon: 'check-square',
};

export const OnboardingScreen1: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const [isPregnant, setIsPregnant] = useState(true);

  const onNext = async (): Promise<void> => {
    await setUserStatus(isPregnant ? USER_ROLE.PREGNANT : USER_ROLE.NEW_PARENT);
    isPregnant ? navigation.navigate('Onboarding2') : navigation.navigate('CustomizeExperience');
  };

  return (
    <Screen testID="registrationScreen" backgroundColor={colors.pearlLusta}>
      <StatusBar barStyle="light-content" />
      <Container
        justifyContent="flex-start"
        alignItems="center"
        pt={SCREEN_HEIGHT * 0.2}
        height={SCREEN_HEIGHT}
      >
        <Text fontSize={7} color={colors.chino} {...fonts.light}>
          {t('onboarding.firstStep.title')}
        </Text>
        <Container
          my={space[8]}
          p={1}
          backgroundColor={colors.white}
          borderRadius={25}
          borderWidth={1}
          borderColor={colors.white}
          minHeight={100}
          width={SCREEN_WIDTH * 0.85}
        >
          <CheckBox
            title={t('onboarding.firstStep.pregnantOption')}
            checked={isPregnant}
            onPress={(): void => setIsPregnant(!isPregnant)}
            {...checkboxStyle}
          />
          <CheckBox
            title={t('onboarding.firstStep.newParentOption')}
            checked={!isPregnant}
            onPress={(): void => setIsPregnant(!isPregnant)}
            {...checkboxStyle}
          />
        </Container>
        <Button
          label={t('onboarding.buttons.next')}
          backgroundColor={colors.wewak}
          width="75%"
          py={2}
          fontSize={6}
          color={colors.white}
          borderRadius={35}
          renderIcon={(): ReactNode => (
            <Container pl={1} height={45} justifyContent="center">
              <Icon name="chevron-right" color={colors.white} size={fontSizes[6]} />
            </Container>
          )}
          iconLeft={false}
          onPress={onNext}
        />
      </Container>
    </Screen>
  );
};
