import React, { FC, ReactNode, useState } from 'react';
import { StatusBar } from 'react-native';
import { Icon } from 'react-native-elements';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import dayjs from 'dayjs';
import { Screen } from '../../../components/Screen';
import { Text } from '../../../components/Text';
import { colors, fonts, fontSizes, space } from '../../../styles';
import { Container } from '../../../components/Container';
import { SCREEN_HEIGHT, t } from '../../../utils';
import { setLastMenstrualPeriod, setEstimatedDueDate } from '../../../utils/Onboarding';
import { Button } from '../../../components/Button';
import { Datepicker } from '../../../components/Datepicker';

const TWENTY_EIGHT_DAYS_AGO = dayjs().subtract(28, 'day');
const AVERAGE_LENGTH_OF_PREGNANCY = 280;

export const OnboardingScreen2: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const [lmp, setLmp] = useState(TWENTY_EIGHT_DAYS_AGO.toDate()); // last menstrual period
  const [edd, setEdd] = useState<Date | undefined>(undefined); // estimated due date
  const [showLmpDatePicker, setShowLmpDatePicker] = useState(false);
  const [showEddDatePicker, setShowEddDatePicker] = useState(false);

  const onNext = async (): Promise<void> => {
    await setLastMenstrualPeriod(dayjs(lmp).format('MM/DD/YYYY'));

    const dueDate = edd
      ? edd.toString()
      : dayjs(lmp)
          .add(AVERAGE_LENGTH_OF_PREGNANCY, 'day')
          .format('MM/DD/YYYY');

    await setEstimatedDueDate(dueDate);

    navigation.navigate('Onboarding3');
  };

  const onBack = (): void => {
    // Tried using both navigation.goBack() and .pop() but neither were functioning, seems to be issue with animated stack
    navigation.navigate('Onboarding1');
  };

  const updateLmp = (value: Date): void => {
    setLmp(value);

    const dueDate = dayjs(value)
      .add(AVERAGE_LENGTH_OF_PREGNANCY, 'day')
      .toDate();

    setEdd(dueDate);
  };

  const updateEdd = (value: Date): void => {
    setEdd(value);
  };

  return (
    <>
      <Screen testID="onboarding2Screen" backgroundColor={colors.pearlLusta}>
        <StatusBar barStyle="light-content" />
        <Container
          justifyContent="flex-start"
          alignItems="center"
          pt={SCREEN_HEIGHT * 0.1}
          height={SCREEN_HEIGHT}
        >
          <Text fontSize={7} color={colors.chino} {...fonts.light}>
            {t('onboarding.secondStep.title')}
          </Text>
          <Container my={8} p={1}>
            <Text fontSize={6} color={colors.gray} textAlign="center">
              {t('onboarding.secondStep.lastMenstrual')}
            </Text>
            <Container flexDirection="row" alignItems="center">
              <Datepicker
                title={t('onboarding.secondStep.lastMenstrual')}
                isVisible={showLmpDatePicker}
                setIsVisible={setShowLmpDatePicker}
                setSelectedDate={updateLmp}
                selectedDate={lmp}
                defaultDate={lmp}
              />
            </Container>
            <Text fontSize={4} pb={2} color={colors.gray} {...fonts.light} textAlign="center">
              {t('onboarding.secondStep.or')}
            </Text>
            <Text fontSize={6} color={colors.gray} textAlign="center">
              {t('onboarding.secondStep.estimatedDueDate')}
            </Text>
            <Container flexDirection="row" alignItems="center">
              <Datepicker
                title={t('onboarding.secondStep.estimatedDueDate')}
                isVisible={showEddDatePicker}
                setIsVisible={setShowEddDatePicker}
                setSelectedDate={updateEdd}
                defaultDate={edd}
                selectedDate={edd}
              />
            </Container>
          </Container>
          <Container fullWidth flexDirection="row" justifyContent="center">
            <Container width="50%" alignItems="flex-end" mx={2}>
              <Button
                label={t('onboarding.buttons.back')}
                backgroundColor={colors.chino}
                width="55%"
                py={2}
                fontSize={6}
                color={colors.white}
                borderRadius={35}
                onPress={onBack}
                renderIcon={(): ReactNode => (
                  <Container pl={1} height={45} justifyContent="center">
                    {
                      //this keeps the buttons consistent looking
                    }
                  </Container>
                )}
              />
            </Container>
            <Container width="50%" alignItems="flex-start" mx={2}>
              <Button
                label={t('onboarding.buttons.next')}
                backgroundColor={colors.wewak}
                width="70%"
                py={2}
                fontSize={6}
                color={colors.white}
                borderRadius={35}
                renderIcon={(): ReactNode => (
                  <Container pl={1} height={45} justifyContent="center">
                    <Icon name="chevron-right" color={colors.white} size={fontSizes[6]} />
                  </Container>
                )}
                iconLeft={false}
                onPress={onNext}
              />
            </Container>
          </Container>
        </Container>
      </Screen>
    </>
  );
};
