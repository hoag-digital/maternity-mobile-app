import React, { FC } from 'react';
import { StatusBar } from 'react-native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { Screen } from '../../../components/Screen';
import { Text } from '../../../components/Text';
import { colors, fonts } from '../../../styles';
import { Container } from '../../../components/Container';
import { SCREEN_HEIGHT, t } from '../../../utils';
import { Button } from '../../../components/Button';

export const OnboardingScreen3: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const onNo = async (): Promise<void> => {
    navigation.navigate('CustomizeExperience');
  };

  const onSignUp = async (): Promise<void> => {
    //Navigate to the form or Mychart or whatever is decided :)
    navigation.navigate('CustomizeExperience');
  };

  return (
    <>
      <Screen testID="onboarding3Screen" backgroundColor={colors.pearlLusta}>
        <StatusBar barStyle="light-content" />
        <Container
          justifyContent="flex-start"
          alignItems="center"
          pt={SCREEN_HEIGHT * 0.1}
          height={SCREEN_HEIGHT}
        >
          <Text fontSize={7} color={colors.chino} {...fonts.light} textAlign="center">
            {t('onboarding.thirdStep.title')}
          </Text>
          <Text fontSize={5} color={colors.gray} textAlign="center" py={9}>
            {t('onboarding.thirdStep.subtitle')}
          </Text>

          <Container fullWidth flexDirection="row" justifyContent="center">
            <Container width="50%" alignItems="flex-end" mx={2}>
              <Button
                label={t('onboarding.thirdStep.buttons.no')}
                backgroundColor={colors.chino}
                width="55%"
                py={2}
                fontSize={6}
                fontWeight="300"
                color={colors.white}
                borderRadius={35}
                onPress={onNo}
              />
            </Container>
            <Container width="50%" alignItems="flex-start" mx={2}>
              <Button
                label={t('onboarding.thirdStep.buttons.signup')}
                backgroundColor={colors.wewak}
                width="70%"
                py={2}
                fontSize={6}
                fontWeight="300"
                color={colors.white}
                borderRadius={35}
                onPress={onSignUp}
              />
            </Container>
          </Container>
        </Container>
      </Screen>
    </>
  );
};
