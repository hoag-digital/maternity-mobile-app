import * as Sentry from '@sentry/react-native';
import gql from 'graphql-tag';
import { useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import React, { FC, ReactElement } from 'react';
import { StatusBar, ActivityIndicator, RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { Icon } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import { TransparencyImageOverlay } from '../../components/TransparencyImageOverlay';

import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Touchable } from '../../components/Touchable';
import GenericNewsImage from '../../assets/images/generic_news.jpg';

import { colors } from '../../styles';
import {
  SMALLEST_ICON_SIZE,
  DEFAULT_ICON_SIZE,
  SCROLLVIEW_BOTTOM_PADDING,
  SCREEN_HEIGHT,
} from '../../utils';
import NewsIcon from '../../assets/images/icons/news.svg';
import { HOME_SCREEN_QUERY } from '../HomeScreen';

const IMAGE_HEIGHT = SCREEN_HEIGHT * 0.4;

export const POST_QUERY = gql`
  query class($postId: ID) {
    post(id: $postId) {
      id
      title
      date
      description
      media {
        fullImage
      }
    }
  }
`;

/**
 * This screen shows the details of a news article (aka wordpress post)
 */
export const PostDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const postId = navigation.getParam('postId');
  const client = useApolloClient();
  let post;

  const goBack = (): void => {
    navigation.goBack();
  };

  const [loadPost, { loading, error, data, refetch }] = useLazyQuery(POST_QUERY, {
    fetchPolicy: 'cache-and-network',
    variables: {
      postId,
    },
  });

  // Until we read from the db, we are reading from the posts cache because the single post endpoint is unpredictable
  try {
    const cachedPostsQuery = client.cache.readQuery({ query: HOME_SCREEN_QUERY }) as Record<
      string,
      any
    >;

    const allCachedPosts = [...cachedPostsQuery.posts, ...cachedPostsQuery.featuredPosts];

    post = allCachedPosts.find((p: Record<string, any>): boolean => p.id === postId);
  } catch (error) {
    Sentry.captureException(error);

    if (!post) {
      loadPost();
    }
  }

  if (error) {
    // TODO: handle graphql errors
  }

  const renderPostDetailsScreenContents = (): ReactElement => {
    if (loading && !Object.keys(data).length) {
      return (
        <Container fill pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          <Container fill fullWidth centerContent>
            <ActivityIndicator />
          </Container>
        </Container>
      );
    }

    const postToRender = post || (data ? data.post : {});
    const { title, description, media } = postToRender;

    // NOTE: Temporarily replacing Post description field with mock content.

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <>
        <StatusBar
          translucent
          animated
          backgroundColor={colors.transparent}
          barStyle="light-content"
        />
        <Container
          backgroundColor={colors.summerSky}
          borderTopRightRadius={7}
          borderBottomRightRadius={7}
          px={3}
          py={1}
          position="absolute"
          top={45}
          left={0}
          flexDirection="row"
          justifyContent="center"
          alignItems="center"
          zIndex={50}
        >
          <NewsIcon height={SMALLEST_ICON_SIZE} width={SMALLEST_ICON_SIZE} />
          <Text fontSize={1} color={colors.white} marginLeft={1}>
            News
          </Text>
        </Container>
        <Touchable
          position="absolute"
          // TODO: future-improvement -> dynamic positioning
          top={45}
          right={20}
          testID="close-icon"
          onPress={goBack}
          bg={colors.white}
          height={DEFAULT_ICON_SIZE}
          width={DEFAULT_ICON_SIZE}
          borderRadius={DEFAULT_ICON_SIZE}
          zIndex={50}
        >
          <Icon name="ios-close" type="ionicon" size={DEFAULT_ICON_SIZE} color={colors.black} />
        </Touchable>
        <TransparencyImageOverlay height={IMAGE_HEIGHT / 2} />
        <FastImage
          accessibilityLabel={`${title} image`}
          style={{
            width: '100%',
            height: IMAGE_HEIGHT,
            resizeMode: 'cover',
          }}
          source={media?.fullImage ? { uri: media.fullImage } : GenericNewsImage}
        />
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
        >
          <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
            <Text pb={2} fontSize={5} fontWeight="bold">
              {title}
            </Text>
            <Text pb={1} fontSize={3} textAlign="justify">
              {description}
            </Text>
          </Container>
        </ScrollView>
      </>
    );
  };

  return (
    <Screen
      testID="post-details-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {renderPostDetailsScreenContents()}
    </Screen>
  );
};

export default PostDetailsScreen;
