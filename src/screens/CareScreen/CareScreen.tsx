import React, { FC, ReactElement, ReactNode } from 'react';
import { ActivityIndicator, FlatList, StyleSheet } from 'react-native';
import { Divider } from 'react-native-elements';
// import LinearGradient from 'react-native-linear-gradient';
import { NavigationStackScreenProps } from 'react-navigation-stack';
// import FastImage from 'react-native-fast-image';

import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Button } from '../../components/Button';
// import { Card } from '../../components/Card';
import { UrgentCareCard } from '../../components/UrgentCareCard';

import { colors } from '../../styles';
import { t, SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
// import { assets } from '../../utils/assets';
// import { BodyText } from '../../components/BodyText';
import TelehealthIcon from '../../assets/images/telehealth.svg';
import HoagLogoIcon from '../../assets/images/hoag-logo-white.svg';

// const KEY_NEAREST = 'nearest';
// const KEY_LEAST_WAIT = 'least-wait';

const LOCATIONS_LIST = [
  // Foothill Ranch
  {
    id: '4435',
    sortOrder: 0,
    locationInformation: {},
  },
  // Woodbury ~5 miles
  {
    id: '1405',
    sortOrder: 1,
    locationInformation: {},
  },
  // Los Olivos 6-10 miles
  {
    id: '1403',
    sortOrder: 2,
    locationInformation: {},
  },
  // Sand Canyon 7-9 miles
  {
    id: '1404',
    sortOrder: 3,
    locationInformation: {},
  },
  // Orchard Hills 7-13 miles
  {
    id: '4274',
    sortOrder: 4,
    locationInformation: {},
  },
  // Tustin Ranch 10-12 miles
  {
    id: '2627',
    sortOrder: 5,
    locationInformation: {},
  },
  // Woodbridge 10-12 miles
  {
    id: '1402',
    sortOrder: 6,
    locationInformation: {},
  },
  // Aliso Viejo 10-16 miles
  {
    id: '1400',
    sortOrder: 7,
    locationInformation: {},
  },
  // Tustin Legacy 12-17 miles
  {
    id: '2628',
    sortOrder: 8,
    locationInformation: {},
  },
  // Newport Coast 19-25 miles
  {
    id: '4436',
    sortOrder: 9,
    locationInformation: {},
  },
  // Newport Beach 21-27 miles
  {
    id: '1406',
    sortOrder: 10,
    locationInformation: {},
  },
  // Huntington Beach 23-38
  {
    id: '1401',
    sortOrder: 11,
    locationInformation: {},
  },
  // Huntington Harbour 26-32
  {
    id: '2626',
    sortOrder: 12,
    locationInformation: {},
  },
];

const headerCardContainerProps = {
  centerContent: false,
  overflow: 'hidden',
  marginBottom: 30,
};

const dividerStyle = { backgroundColor: colors.silver, marginVertical: 15 };

const headerImageStyle = {
  width: '100%',
  height: '100%',
  resizeMode: 'cover',
  ...StyleSheet.absoluteFillObject,
};

const WAIT_TIMES_QUERY = gql`
  query waitTimes {
    waitTimes {
      hospital {
        id
        name
        todaysBusinessHours
      }
      waits {
        nextAvailableVisit
        currentWait
        queueLength
        queueTotal
      }
    }
  }
`;

export const CareScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const {
    data: waitTimesData = {},
    loading: waitTimesLoading,
    networkStatus: waitTimesNetworkStatus,
    refetch: refetchWaitTimes,
  } = useQuery(WAIT_TIMES_QUERY, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  const locationWaitTimes = (waitTimesData && waitTimesData.waitTimes) || [];

  // TODO: re-enable when we allow sort by current location
  // const [activeForm, setActiveForm] = React.useState(KEY_NEAREST); // was 'for-me' valid?

  // let locations = MOCK_URGENT_CARE_LOCATIONS;

  // if (activeForm === KEY_LEAST_WAIT) {
  //   locations = MOCK_URGENT_CARE_LOCATIONS.concat().sort((l1, l2): number => l1.wait - l2.wait);
  // } else if (activeForm === KEY_NEAREST) {
  //   locations = MOCK_URGENT_CARE_LOCATIONS.concat().sort(
  //     (l1, l2): number => l1.distance - l2.distance
  //   );
  // }

  // const TAB_BUTTONS = [
  //   {
  //     label: t('care.nearestLocation'),
  //     key: KEY_NEAREST,
  //     icon: 'map-marker',
  //   },
  //   {
  //     label: t('care.leastWait'),
  //     key: KEY_LEAST_WAIT,
  //     icon: 'clock',
  //   },
  // ];

  /**TODO remove this if we are not going to have physician scheduling, unclear as of this PR */
  // const renderPhysicianSchedulingCard = (): ReactNode => {
  //   return (
  //     <>
  //       <Text testID="care-screen-header" fontWeight="bold" fontSize={5}>
  //         {t('care.header')}
  //       </Text>
  //       <Divider style={dividerStyle} />
  //       <Card
  //         containerProps={headerCardContainerProps}
  //         renderTop={(): ReactNode => (
  //           <Container position="relative" minHeight={150}>
  //             <FastImage source={assets.images.careHeader} style={headerImageStyle} />
  //           </Container>
  //         )}
  //         renderBottom={(): ReactNode => (
  //           <LinearGradient
  //             start={{ x: 0, y: 0 }}
  //             end={{ x: 1, y: 0 }}
  //             style={{ alignItems: 'center' }}
  //             colors={gradients.whiteSolitudeLight}
  //           >
  //             <Container width="70%">
  //               <BodyText textAlign="center" my={4}>
  //                 {t('care.scheduleAppointmentCopy')}
  //               </BodyText>
  //             </Container>
  //             <Container flexDirection="row" width="100%" justifyContent="space-between">
  //               <Button
  //                 backgroundColor={colors.malibu}
  //                 label={t('care.getStarted')}
  //                 color={colors.white}
  //                 borderRadius={5}
  //                 py={3}
  //                 mb={5}
  //                 flex={1}
  //                 mx={4}
  //                 onPress={(): void => {
  //                   navigation.navigate('Schedule Foothill Appointment');
  //                 }}
  //               />
  //               <Button
  //                 backgroundColor={colors.violet}
  //                 label={t('care.womens')}
  //                 color={colors.white}
  //                 borderRadius={5}
  //                 py={3}
  //                 mb={5}
  //                 flex={1}
  //                 mr={4}
  //                 onPress={(): void => {
  //                   navigation.navigate('Womens');
  //                 }}
  //               />
  //             </Container>
  //           </LinearGradient>
  //         )}
  //       />
  //     </>
  //   );
  // };

  /**TODO remove this if we keep the above implementation leaving here just in case we go with the buttons, should know very soon */
  const renderCareOptions = (): ReactNode => {
    return (
      <>
        {/* <Text testID="care-screen-header" fontWeight="bold" fontSize={5}>
          {t('care.header')}
        </Text> */}
        <Button
          backgroundColor={colors.cerulean}
          label={t('care.telehealth')}
          color={colors.white}
          fontSize={3}
          fontWeight="700"
          borderRadius={5}
          py={3}
          mt={25}
          mb={5}
          flex={1}
          mx={4}
          onPress={(): void => {
            navigation.navigate('Telehealth');
          }}
          renderIcon={(): ReactNode => (
            <Container mr={1} height={35}>
              {/* <TelehealthIcon /> */}
            </Container>
          )}
        />
        <Button
          backgroundColor={colors.terra}
          label={t('care.selfAssessment')}
          color={colors.white}
          borderRadius={5}
          py={3}
          mt={10}
          mb={5}
          flex={1}
          mx={4}
          fontSize={3}
          fontWeight="700"
          onPress={(): void => {
            navigation.navigate('Self Assessment');
          }}
          renderIcon={(): ReactNode => (
            // Leaving this to keep button the same dimensions as the telehealth link
            <Container height={35} />
            // <Container mr={1} maxHeight={36}>
            //   <HoagLogoIcon height={35} />
            // </Container>
          )}
        />
      </>
    );
  };

  const renderUrgentCareHeader = (): ReactNode => {
    return (
      <Container flexDirection="row" alignItems="center" mb={4}>
        <Text testID="careScreenSubHeading" fontWeight="bold" fontSize={4} pr={2}>
          {t('care.subhead')}
        </Text>

        <Divider
          style={{
            flex: 1,
            backgroundColor: colors.silver,
          }}
        />
      </Container>

      /* TODO: re-enable when we allow sort by location */
      /* <Container alignItems="center" flexDirection="row" justifyContent="center" mb={5}>
          {TAB_BUTTONS.map(button => (
            <Button
              key={button.key}
              backgroundColor={button.key === activeForm ? colors.balticSea : colors.whiteSmoke}
              mx={1}
              label={button.label}
              color={button.key === activeForm ? colors.white : colors.balticSea50}
              borderRadius={5}
              onPress={(): void => {
                setActiveForm(button.key);
              }}
              pl={2}
              pr={3}
              py={2}
              renderIcon={(): ReactNode => (
                <Container mr={1}>
                  <Icon
                    name={button.icon}
                    type="material-community"
                    color={button.key === activeForm ? colors.white : colors.balticSea50}
                  />
                </Container>
              )}
            />
          ))}
        </Container> */
    );
  };

  /**
   * The header of the FlatList
   */
  const renderHeaderContents = (): ReactElement => {
    return (
      <Container pt={3} px={5} bg={colors.white}>
        {/* {renderPhysicianSchedulingCard()} */}
        {renderCareOptions()}
        {renderUrgentCareHeader()}
      </Container>
    );
  };

  /** Renders an Urgent Care location "card" */
  const renderCareLocation = ({ item }): ReactElement | null => {
    if (!item.locationInformation) return null;

    const { id, name, todaysBusinessHours } = item.locationInformation.hospital;
    const { currentWait } = item.locationInformation.waits;

    return (
      <Container pt={5} px={5} backgroundColor={colors.sky}>
        <UrgentCareCard
          id={id}
          name={name}
          closes={todaysBusinessHours}
          wait={currentWait}
          onPress={(): void => {
            navigation.navigate('Appointment Form', {
              location: item.locationInformation.hospital,
            });
          }}
        />
      </Container>
    );
  };

  /**
   * The footer in the FlatList. Allows for extra space at the bottom
   */
  const renderFooterContents = (): ReactElement => {
    return <Container bg={colors.sky} fullWidth pb={SCROLLVIEW_BOTTOM_PADDING} />;
  };

  const renderScreenContents = (): ReactElement => {
    if (waitTimesLoading && !Object.keys(waitTimesData).length) {
      return (
        <Container fill pt={4} px={5} bg={colors.white}>
          {/* <Text testID="care-screen-header" fontWeight="bold" fontSize={5}>
            {t('care.header')}
          </Text> */}
          <Container fill fullWidth centerContent>
            <ActivityIndicator />
          </Container>
        </Container>
      );
    }

    // add the api data for each location to the hardcoded list and sort...
    // NOTE: the usage of a hardcoded list was added to meet a deadline and should eventually be
    // replaced with completely api-driven data
    const hydratedLocationsList = LOCATIONS_LIST.map((location): any => {
      const dataForLocation = locationWaitTimes.find((locationWaitTime): any => {
        return locationWaitTime.hospital.id === location.id;
      });

      if (dataForLocation) {
        return {
          ...location,
          locationInformation: dataForLocation,
        };
      }

      return location;
    }).sort((a, b) => {
      return a.sortOrder - b.sortOrder;
    });

    // networkStatus 4 indicates a refetch while 1 indicates the initial query fetch...
    // only show the scrollview loading spinner on an explicit user-requested refetch
    const isRefetching = waitTimesLoading && waitTimesNetworkStatus === 4;

    return (
      <FlatList
        data={hydratedLocationsList}
        keyExtractor={(item, index): string => `${item.id}-${index}`}
        renderItem={renderCareLocation}
        ListHeaderComponent={renderHeaderContents}
        ListFooterComponent={renderFooterContents}
        refreshing={isRefetching}
        onRefresh={refetchWaitTimes}
      />
    );
  };

  return (
    <Screen
      testID="care-screen"
      screenHeader
      backgroundColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      mb={5}
    >
      {renderScreenContents()}
    </Screen>
  );
};
