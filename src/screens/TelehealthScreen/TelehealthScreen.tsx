import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t } from '../../utils';
import ENV from '../../env';
import { REMOVE_HEADER_FOOTERS } from '../../lib/InjectedJavascript';

export const TelehealthScreen: FC<NavigationStackScreenProps> = () => {
  return (
    <Screen
      testID="telehealth-screen"
      modalHeader
      screenTitle={t('care.telehealth')}
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      <WebView
        source={{ uri: ENV.TELEHEALTH_URL }}
        containerStyle={StyleSheet.absoluteFillObject}
        startInLoadingState
        injectedJavaScript={REMOVE_HEADER_FOOTERS}
      />
    </Screen>
  );
};
