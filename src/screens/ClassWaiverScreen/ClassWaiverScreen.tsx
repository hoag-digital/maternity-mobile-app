/**
 * THIS FILE IS NOT CURRENTLY IN-USE - ratkinson
 */
import React, { FC } from 'react';
import { ScrollView } from 'react-native';
import { NavigationStackScreenProps } from 'react-navigation-stack';

import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { SignatureForm } from '../../components/SignatureForm';
import { colors } from '../../styles';
import { t } from '../../utils';

export const ClassWaiverScreen: FC<NavigationStackScreenProps> = () => {
  const MOCK_NAME = 'Ryan Atkinson';

  const MOCK_COPY =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at magna pretium, pellentesque velit et, tincidunt ligula. Mauris sollicitudin ante ut auctor volutpat. Nam cursus malesuada felis ac rhoncus.';

  const MOCK_COPY_EXTENDED = `${MOCK_COPY} ${MOCK_COPY}`;

  const submitWaiver = (): void => {};

  return (
    <Screen
      testID="class-waiver-screen"
      modalHeader
      screenTitle={t('classes.waiver.header')}
      backgroundColor={colors.snow}
      headerColor={colors.cinnabar}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {/* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */}
      <ScrollView scrollIndicatorInsets={{ right: 1 }}>
        <Container px={30} py={5}>
          <Text py={3} fontWeight="bold">
            {MOCK_COPY_EXTENDED}
          </Text>
          <Text py={3}>{MOCK_COPY_EXTENDED}</Text>
          <Text py={3}>{MOCK_COPY_EXTENDED}</Text>
        </Container>
      </ScrollView>
      <SignatureForm name={MOCK_NAME} onSubmit={submitWaiver} />
    </Screen>
  );
};
