import * as Sentry from '@sentry/react-native';
import gql from 'graphql-tag';
import { useQuery, useMutation } from '@apollo/react-hooks';
import React, { FC, useState, ReactElement } from 'react';
import { StatusBar, ActivityIndicator, RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationActions, StackActions } from 'react-navigation';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { Divider, Icon } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from '@react-native-community/async-storage';
import dayjs from 'dayjs';

import { NotificationsConsumer } from '../../services/notifications';
import { ClassesCategoryLabel } from '../../components/ClassesCategoryLabel';
import { ClassReservationModal } from '../../components/ClassReservationModal';
import { EventListItem } from '../../components/EventListItem';
import { VirtualEventListItem } from '../../components/VirtualEventListItem';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Touchable } from '../../components/Touchable';
import { TransparencyImageOverlay } from '../../components/TransparencyImageOverlay';

import { colors } from '../../styles';
import { Event } from '../../types';
import {
  ASYNCSTORAGE_FCM_KEY,
  DEFAULT_ICON_SIZE,
  SCROLLVIEW_BOTTOM_PADDING,
  SCREEN_HEIGHT,
  t,
} from '../../utils';
import GenericClassImage from '../../assets/images/generic_class.jpg';
import { ErrorModal } from '../../components/ErrorModal';

const IMAGE_HEIGHT = SCREEN_HEIGHT * 0.4;
const dividerStyle = { backgroundColor: colors.silver, marginVertical: 15 };

const classIsFull = (event: Event): boolean => !event.spacesRemaining;

const isRegistered = (events: Event[], postId): boolean =>
  events.some(event => event.postId === postId);

// Note that eventGroups are artificial groupings and thus do not possess ids correlating to any db
// entity. We are using the name of an avent group as its unique identifier.
export const HOAG_EVENT_GROUP_QUERY = gql`
  query eventGroup($eventGroupName: String) {
    eventGroup(eventGroupId: $eventGroupName) {
      name
      description
      thumbnail
      events {
        id
        postId
        spacesRemaining
        totalSpaces
        startTime
        endTime
        date
        eventStartDate
        youtubeLiveLink
        isVirtual
      }
    }
  }
`;

export const MY_SCHEDULED_EVENTS_QUERY = gql`
  query scheduledEvents {
    myEvents {
      postId
      date
      eventId
      name
      eventStartDate
      eventStart
      startTime
      endTime
    }
  }
`;

// Note that an event is really a wordpress "post" with extra properties. In order to register for an
// event, we use the id of the underlying wordpress post entity.
export const REGISTER_USER_FOR_EVENT_MUTATION = gql`
  mutation EventRegistration($postId: String) {
    eventRegistration(postId: $postId)
  }
`;

/**
 * This screen displays the expanded details of an eventGroup (referred to as a "class" to the user)
 * including its full description, image, and available slot times for user registration.
 */
export const ClassDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const eventGroupName = navigation.getParam('eventGroupName');
  const eventCategoryName = navigation.getParam('eventCategoryName');
  const eventCategoryId = navigation.getParam('eventCategoryId');

  const [registerForEvent, { loading: isRegistering }] = useMutation(
    REGISTER_USER_FOR_EVENT_MUTATION,
    {
      refetchQueries: [
        {
          query: MY_SCHEDULED_EVENTS_QUERY,
        },
      ],
      // Don't complete the mutation until the new data is done refetching
      awaitRefetchQueries: true,
    }
  );

  const {
    error: eventGroupError,
    data: eventGroupData = {},
    loading: eventGroupLoading,
    refetch: refetchEventGroup,
  } = useQuery(HOAG_EVENT_GROUP_QUERY, {
    fetchPolicy: 'cache-and-network',
    variables: {
      eventGroupName,
    },
  });

  const {
    data: myEventsData = {
      myEvents: [],
    },
  } = useQuery(MY_SCHEDULED_EVENTS_QUERY, {
    fetchPolicy: 'cache-and-network',
  });

  // Note that a "classId" is actually an event.postId (the wordpress post id belonging to the wordpress event)
  const [selectedClassId, selectClassId] = useState(null);
  const [reservationModalVisible, setReservationModalVisible] = useState(false);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const goBack = (): void => {
    navigation.goBack();
  };

  // This function opens the class reservation modal (Step 1)
  // NOTE: this "classId" is actually the "postId" of the individual event
  const initiateClassReservation = (postId?: string | null): void => {
    if (!postId) return;

    selectClassId(postId);
    setReservationModalVisible(true);
  };

  // This is the callback function invoked when a class is reserved from the modal (Step 2)
  const onReserveClass = async (postId: string, notificationsContext: any): Promise<string> => {
    // TODO: put in a try/catch block and handle failure, when we have an error message UI.
    try {
      const success = await registerForEvent({
        variables: {
          postId,
        },
      });

      // TODO: move to pulling the reservedClass from refetched scheduled events as a security measure
      // const hoagClass =
      //   myEventsData?.myEvents?.find((event: Event): boolean => event.postId === classId) ??
      //   ({} as Event);

      const reservedClass =
        eventGroupData?.eventGroup?.events?.find(
          (event: Event): boolean => event.postId === postId
        ) ?? ({} as Event);

      setReservationModalVisible(false);

      navigation.navigate('ReservationConfirmation', {
        type: 'class',
        reservation: {
          date: reservedClass.date,
          startTime: reservedClass.startTime,
          endTime: reservedClass.endTime,
        },
        actions: {
          primary: StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Home',
                action: NavigationActions.navigate({
                  routeName: 'Schedule',
                }),
              }),
            ],
          }),
          secondary: NavigationActions.navigate({
            routeName: 'ClassCategories',
          }),
        },
      });

      const fcmToken = await AsyncStorage.getItem(ASYNCSTORAGE_FCM_KEY);

      // if there is no fcmToken saved, the notifications need to be initialized
      if (!fcmToken) {
        notificationsContext.setNotificationsShouldBeInitialized(true);
      }

      Promise.resolve(success);
    } catch (error) {
      Sentry.captureException(error);

      const message =
        error.toString() === `Error: GraphQL error: This Class is at capacity`
          ? t('errors.classCapacity')
          : t('errors.general');

      setErrorMessage(message);
      setReservationModalVisible(false);
      setTimeout(() => {
        // Error Modal will not appear when other modal is present and it takes a moment
        setErrorModalVisible(true);
      }, 400);
    }
  };

  // TODO: add a use effect to wipe the selected class time when the modal becomes invisible...
  const renderClassReservationModal = (): ReactElement => {
    const eventGroup = eventGroupData ? eventGroupData.eventGroup : {};
    const { name, thumbnail, events } = eventGroup;

    // TODO: test this for synchronicity issues
    const selectedClass = events.find(({ postId }) => postId === selectedClassId) || ({} as Event);

    return (
      <NotificationsConsumer>
        {(notificationsContext): ReactElement => {
          return (
            <ClassReservationModal
              isRegistering={isRegistering}
              className={name}
              isVisible={reservationModalVisible}
              setIsVisible={setReservationModalVisible}
              classImageUrl={thumbnail}
              classTimeId={selectedClassId}
              startTime={selectedClass.startTime}
              endTime={selectedClass.endTime}
              date={selectedClass.date}
              onReserveClass={(postId): Promise<string> =>
                onReserveClass(postId, notificationsContext)
              }
            />
          );
        }}
      </NotificationsConsumer>
    );
  };

  const renderErrorModal = (): ReactElement => {
    return (
      <ErrorModal
        error={errorMessage}
        isVisible={errorModalVisible}
        setIsVisible={setErrorModalVisible}
        buttonColor={colors.malibu}
      />
    );
  };

  const renderEventListItem = (event: Event): ReactElement | null => {
    // temprorarily filter out events that already occurred
    // todo: move logic to the backend
    if (dayjs(event.startTime).isBefore(dayjs())) {
      return null;
    }

    const myEvents = myEventsData ? myEventsData.myEvents : [];

    const classId = event.postId;

    // we are passing all accumulated navigation params so that we can recreate the navigation
    // ...history when redirected to and from login/account creation screens
    const navigationParams = {
      eventCategoryName,
      eventGroupName,
      eventCategoryId,
    };

    // There is separate UI behavior for virtual and in-facility event types
    if (event.isVirtual) {
      return (
        <VirtualEventListItem
          key={event.id}
          {...event}
          isFull={classIsFull(event)}
          isRegistered={isRegistered(myEvents, classId)}
          initiateClassReservation={initiateClassReservation}
          spacesRemaining={event.spacesRemaining}
          navigationParams={navigationParams}
        />
      );
    }

    return (
      <EventListItem
        key={event.id}
        {...event}
        isFull={classIsFull(event)}
        isRegistered={isRegistered(myEvents, classId)}
        initiateClassReservation={initiateClassReservation}
        spacesRemaining={event.spacesRemaining}
        navigationParams={navigationParams}
      />
    );
  };

  const renderClassDetailsScreenContents = (): ReactElement => {
    if (eventGroupLoading && !Object.keys(eventGroupData).length) {
      return (
        <Container fill pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          <Container fill fullWidth centerContent>
            <ActivityIndicator />
          </Container>
        </Container>
      );
    }

    const eventGroup = eventGroupData ? eventGroupData.eventGroup : {};

    const { name, description, thumbnail, events } = eventGroup;

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <>
        <StatusBar
          translucent
          animated
          backgroundColor={colors.transparent}
          barStyle="light-content"
        />
        {renderClassReservationModal()}
        {renderErrorModal()}
        <Touchable
          position="absolute"
          // TODO: future-improvement -> dynamic positioning
          top={45}
          right={20}
          testID="close-icon"
          onPress={goBack}
          bg={colors.white}
          height={DEFAULT_ICON_SIZE}
          width={DEFAULT_ICON_SIZE}
          borderRadius={DEFAULT_ICON_SIZE}
          zIndex={50}
        >
          <Icon name="ios-close" type="ionicon" size={DEFAULT_ICON_SIZE} color={colors.black} />
        </Touchable>
        <Container position="relative">
          <TransparencyImageOverlay height={IMAGE_HEIGHT / 2} />
          <FastImage
            accessibilityLabel={`${name} image`}
            style={{
              width: '100%',
              height: IMAGE_HEIGHT,
              resizeMode: 'cover',
            }}
            source={thumbnail ? { uri: thumbnail } : GenericClassImage}
          />
          <ClassesCategoryLabel position="absolute" top={10 + getStatusBarHeight()} left={0} />
        </Container>
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={
            <RefreshControl refreshing={eventGroupLoading} onRefresh={refetchEventGroup} />
          }
        >
          <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
            <Text pb={2} fontSize={5} fontWeight="bold">
              {name}
            </Text>
            <Text pb={2} fontSize={3} textAlign="justify">
              {description}
            </Text>
            <Divider style={dividerStyle} />
            {events.map(renderEventListItem)}
          </Container>
        </ScrollView>
      </>
    );
  };

  if (eventGroupError) {
    // TODO: handle graphql errors
  }

  return (
    <Screen
      testID="class-details-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {renderClassDetailsScreenContents()}
    </Screen>
  );
};
