import React, { FC, useEffect } from 'react';
import styled from '@emotion/native';
import { NavigationStackScreenProps } from 'react-navigation-stack';

import { ImageBackground, StatusBar, StyleSheet } from 'react-native';
import Logo from '../../assets/images/hoag-logo-white.svg';
import welcomeImg from '../../assets/images/welcome-bg.png';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Container } from '../../components/Container';
import { t } from '../../utils';
import { colors, fonts } from '../../styles';
import { getOnboarded } from '../../utils/Onboarding';

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

export const WelcomeScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  useEffect(() => {
    setTimeout(async () => {
      const onboarded = await getOnboarded();
      onboarded ? navigation.navigate('Home') : navigation.navigate('Onboarding1');
    }, 1500);
  });

  return (
    <BackgroundImage source={welcomeImg} resizeMode="stretch">
      <Screen testID="welcomeScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <Container justifyContent="center" alignItems="center" pt={100}>
          <Logo height="100" width="100" />
          <Text fontSize={5} color={colors.white} py={10} {...fonts.light}>
            {t('welcome.title')}
          </Text>
          <Text fontSize={6} color={colors.chino} py={10} {...fonts.light}>
            {t('welcome.subtitle')}
          </Text>
        </Container>
      </Screen>
    </BackgroundImage>
  );
};
