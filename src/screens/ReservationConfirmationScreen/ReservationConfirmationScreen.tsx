import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import {
  ImageBackground,
  StyleSheet,
  StatusBar,
  ImageBackgroundProps,
  StatusBarProps,
  ViewStyle,
  TextStyle,
} from 'react-native';
import styled from '@emotion/native';

import dayjs from 'dayjs';
import { NavigationAction } from 'react-navigation';
import { Container } from '../../components/Container';
import { Touchable } from '../../components/Touchable';
import { Button } from '../../components/Button';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { colors } from '../../styles';
import { t, SMALL_HITSLOP } from '../../utils';

import biocircuitBgImg from '../../assets/images/biocircuit/confirmation.jpg';
import classesBgImg from '../../assets/images/classes/confirmation.jpg';

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

type ReservationType = 'program' | 'class';

export interface ReservationConfirmationScreenNavigationParams {
  type: ReservationType;
  reservation: {
    date: Date;
    startTime: Date;
    endTime: Date;
  };
  actions: {
    primary: NavigationAction;
    secondary: NavigationAction;
  };
}

interface ReservationTheme {
  iconColor: TextStyle['color'];
  buttonColor: TextStyle['color'];
  buttonTextColor: TextStyle['color'];
  textColor: TextStyle['color'];
  statusBarStyle: StatusBarProps['barStyle'];
  backgroundImage: ImageBackgroundProps['source'];
  backgroundColor: ViewStyle['backgroundColor'];
}

interface ReservationContent {
  [key: string]: string;
}

const getTheme = (type: ReservationType): ReservationTheme => {
  switch (type) {
    case 'class':
      return {
        iconColor: colors.lightGreen,
        buttonColor: colors.lightGreen,
        buttonTextColor: colors.black,
        textColor: colors.white,
        statusBarStyle: 'light-content',
        backgroundImage: classesBgImg,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      };
    case 'program':
      return {
        iconColor: colors.tangerineYellow,
        buttonColor: colors.tangerineYellow,
        buttonTextColor: colors.black,
        textColor: colors.white,
        statusBarStyle: 'light-content',
        backgroundImage: biocircuitBgImg,
        backgroundColor: 'transparent',
      };
  }
};

const getContent = (type: ReservationType): ReservationContent => {
  switch (type) {
    case 'class':
      return {
        title: t('classes.confirmation.title'),
        subtitle: t('classes.confirmation.subtitle'),
        primaryActionLabel: t('classes.confirmation.primaryActionLabel'),
        secondaryActionLabel: t('classes.confirmation.secondaryActionLabel'),
      };
    case 'program':
      return {
        title: t('programs.confirmation.title'),
        subtitle: t('programs.confirmation.subtitle'),
        primaryActionLabel: t('programs.confirmation.primaryActionLabel'),
        secondaryActionLabel: t('programs.confirmation.secondaryActionLabel'),
      };
  }
};

export const ReservationConfirmationScreen: FC<NavigationStackScreenProps<
  ReservationConfirmationScreenNavigationParams
>> = ({ navigation }) => {
  const type = navigation.getParam('type');
  const reservation = navigation.getParam('reservation');
  const actions = navigation.getParam('actions');

  const theme = getTheme(type);
  const content = getContent(type);

  const onSelectAction = action => (): void => {
    if (action) {
      navigation.dispatch(action);
    }
  };

  return (
    <BackgroundImage source={theme.backgroundImage} resizeMode="cover">
      <Screen transparent backgroundColor={theme.backgroundColor} fill>
        <StatusBar barStyle={theme.statusBarStyle} />
        <Container fill fullWidth justifyContent="space-around">
          <Container />
          <Container fullWidth centerContent>
            <Icon name="checkcircleo" type="antdesign" size={160} color={theme.iconColor} />
            <Container py={5} centerContent>
              <Text fontSize={4} color={theme.textColor}>
                {content.title}
              </Text>
              <Text fontSize={4} color={theme.textColor}>
                {content.subtitle}
              </Text>
              {reservation ? (
                <>
                  <Text fontSize={3} color={theme.textColor} pt={7} fontWeight="bold">
                    {dayjs(reservation.date).format('dddd, MMMM DD')}
                  </Text>
                  <Text fontSize={3} color={theme.textColor} pt={2}>
                    {dayjs(reservation.startTime).format('hh:mm A')}-
                    {dayjs(reservation.endTime).format('hh:mm A')}
                  </Text>
                </>
              ) : null}
            </Container>
          </Container>
          <Container centerContent fullWidth px={5}>
            {actions.primary ? (
              <Button
                fullWidth
                backgroundColor={theme.buttonColor}
                label={content.primaryActionLabel}
                color={theme.buttonTextColor}
                borderRadius={5}
                py={3}
                onPress={onSelectAction(actions.primary)}
              />
            ) : null}
            {actions.secondary ? (
              <Touchable onPress={onSelectAction(actions.secondary)} py={3} hitSlop={SMALL_HITSLOP}>
                <Text fontSize={2} color={theme.textColor}>
                  {content.secondaryActionLabel}
                </Text>
              </Touchable>
            ) : null}
          </Container>
        </Container>
      </Screen>
    </BackgroundImage>
  );
};
