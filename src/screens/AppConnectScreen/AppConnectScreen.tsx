import React, { ReactElement } from 'react';
import { Divider } from 'react-native-elements';

import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';

import { colors, gradients } from '../../styles';
import { Text } from '../../components/Text';

import myChart from '../../assets/images/myChartLogo.png';
import circleLogo from '../../assets/images/circleLogo.png';
import wellDLogo from '../../assets/images/wellDlogo.png';
import { AppConnect } from '../../components/AppConnect';

export function AppConnectScreen(): ReactElement {
  return (
    <Screen
      testID="app-connect-screen"
      screenHeader
      backgroundColor={colors.transparent}
      justifyContent="flex-start"
      paddingTop={1}
      marginHorizontal={20}
    >
      <Container fill fullWidth>
        <Text fontSize={24} fontWeight="bold">
          AppConnect
        </Text>
        <Divider style={{ backgroundColor: colors.silver, marginVertical: 15 }} />
        <AppConnect
          app="My Chart"
          subtitle="Epic"
          appReview="#3 in Medical"
          logo={myChart}
          gradient={gradients.mychart}
          reviewColor={colors.myChartReview}
        />
        <AppConnect
          app="Circle"
          subtitle="Circle Medical Tech"
          appReview="200 reviews"
          logo={circleLogo}
          gradient={gradients.circle}
          reviewColor={colors.circleReview}
        />
        <AppConnect
          app="WellD"
          subtitle="Epic"
          appReview="135 reviews"
          logo={wellDLogo}
          gradient={gradients.wellD}
          reviewColor={colors.wellDReview}
        />
      </Container>
    </Screen>
  );
}
