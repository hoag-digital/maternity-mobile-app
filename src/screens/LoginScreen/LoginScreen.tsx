import * as Sentry from '@sentry/react-native';
import gql from 'graphql-tag';
import React, { useState, FC, ReactElement } from 'react';
import { useMutation } from '@apollo/react-hooks';
import {
  ImageBackground,
  StyleSheet,
  Keyboard,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';
import { NavigationSwitchScreenProps, NavigationAction } from 'react-navigation';
import { WebView } from 'react-native-webview';
import styled from '@emotion/native';
import { Icon } from 'react-native-elements';

import { NotificationsConsumer } from '../../services/notifications';
import Logo from '../../assets/images/hoag-logo-white.svg';
import { Login } from '../../components/Login';
import { Screen } from '../../components/Screen';
import bgImage from '../../assets/images/hoag-background.jpg';
import { Container } from '../../components/Container';
import { Text } from '../../components/Text';
import { AuthConsumer } from '../../lib/AuthConsumer';
import { colors } from '../../styles';
import ENV from '../../env';
import { Touchable } from '../../components/Touchable';
import { LARGEST_ICON_SIZE, DEFAULT_HITSLOP } from '../../utils';

const { PASSWORD_RESET_URL, USE_SENTRY } = ENV;

export const LOGIN_MUTATION = gql`
  mutation login($email: String, $password: String) {
    login(email: $email, password: $password) {
      token
      user {
        id
        email
      }
    }
  }
`;

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

type NavigationRedirectParam = {
  action: NavigationAction;
  callback?(): void;
};

interface LoginNavigationParams {
  redirect: NavigationRedirectParam;
}

export const LoginScreen: FC<NavigationSwitchScreenProps<LoginNavigationParams>> = ({
  navigation,
}) => {
  const [showPasswordResetWebview, setShowPasswordResetWebView] = useState(false);
  const [loading, setLoading] = useState(false);
  const [login] = useMutation(LOGIN_MUTATION);
  const [loginError, setLoginError] = useState(false);

  const loginPress = async (
    auth: any,
    email: string,
    password: string,
    notificationsContext: any
  ): Promise<void> => {
    setLoading(true);
    Keyboard.dismiss();

    try {
      const { data: loginData } = await login({
        variables: {
          email,
          password,
        },
      });

      const token = loginData?.login?.token;
      const user = loginData?.login?.user;

      if (token) {
        await auth.saveToken(token);

        if (USE_SENTRY && user) {
          Sentry.setUser({ id: user.id });
        }

        setLoading(false);

        const redirect = navigation.getParam('redirect');

        if (redirect) {
          navigation.dispatch(redirect.action);
          redirect.callback?.();
        } else {
          navigation.navigate('Home');
        }

        notificationsContext.setNotificationsShouldBeInitialized(true);
      } else {
        setLoginError(true);
      }
    } catch (error) {
      Sentry.captureException(error);
      setLoginError(true);
    } finally {
      setLoading(false);
    }
  };

  const registrationPress = (): void => {
    navigation.navigate('Registration', { redirect: navigation.getParam('redirect') });
  };

  const forgotPwdPress = (): void => {
    setShowPasswordResetWebView(true);
  };

  if (showPasswordResetWebview) {
    return (
      <>
        <WebView
          source={{ uri: PASSWORD_RESET_URL }}
          containerStyle={StyleSheet.absoluteFillObject}
        />
        <Container position="absolute" fullWidth centerContent bottom={0} pb={50} px={5}>
          <Text fontSize={2} fontWeight="bold" textAlign="center" color={colors.terra}>
            After resetting your password, please close and reopen the Hoag app.
          </Text>
        </Container>
      </>
    );
  }

  return (
    <BackgroundImage source={bgImage} resizeMode="cover">
      <Screen testID="registrationScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <KeyboardAvoidingView behavior="position" enabled>
          <Container
            flexDirection="row"
            justifyContent="flex-start"
            alignItems="center"
            padding={20}
          >
            <Touchable
              testID="back-icon"
              onPress={(): boolean => navigation.navigate('Home')}
              hitSlop={DEFAULT_HITSLOP}
            >
              <Icon
                name="ios-arrow-round-back"
                type="ionicon"
                size={LARGEST_ICON_SIZE}
                color={colors.white}
              />
            </Touchable>
            <Container mx="auto">
              <Logo />
            </Container>
            <Container width={30} />
          </Container>
          <NotificationsConsumer>
            {(notificationsContext): ReactElement => {
              return (
                <AuthConsumer>
                  {(auth): ReactElement => {
                    return (
                      <Login
                        loginPress={(email: string, password: string): Promise<void> =>
                          loginPress(auth, email, password, notificationsContext)
                        }
                        registrationPress={registrationPress}
                        forgotPasswordPress={forgotPwdPress}
                        loginLoading={loading}
                        loginError={loginError}
                      />
                    );
                  }}
                </AuthConsumer>
              );
            }}
          </NotificationsConsumer>
        </KeyboardAvoidingView>
      </Screen>
    </BackgroundImage>
  );
};
