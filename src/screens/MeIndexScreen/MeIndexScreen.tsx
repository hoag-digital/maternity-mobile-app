import React, { FC } from 'react';

import { NavigationStackScreenProps } from 'react-navigation-stack';

import { StatusBar } from 'react-native';

import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Container } from '../../components/Container';

import { colors, fonts } from '../../styles';

export const MeIndexScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  return (
    <Screen testID="meIndexScreen" bg={colors.pearlLusta} screenHeader fullWidth flex={1}>
      <StatusBar barStyle="light-content" />
      <Container justifyContent="center" alignItems="center" pt={100}>
        <Text fontSize={5} color={colors.chino} py={10} {...fonts.light}>
          Me Index Placeholder Screen
        </Text>
      </Container>
    </Screen>
  );
};
