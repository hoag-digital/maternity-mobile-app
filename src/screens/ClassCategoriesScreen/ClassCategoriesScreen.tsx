import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import React, { FC, ReactElement } from 'react';
import { ActivityIndicator, RefreshControl } from 'react-native';
import { Divider } from 'react-native-elements';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { ScrollView } from 'react-native-gesture-handler';

import { Container } from '../../components/Container';
import { ClassCategoryCard } from '../../components/Card';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';

import { colors } from '../../styles';
import { SCROLLVIEW_BOTTOM_PADDING, t } from '../../utils';
import { ClassCategory } from '../../types';

export const CLASS_CATEGORIES_QUERY = gql`
  query eventCategories {
    eventCategories {
      id
      name
      description
      count
      thumbnail
    }
  }
`;

/**
 * Displays a list of the wordpress event metadata fields: "event-category". These are branded as
 * "class categories" to the user of the app, and pressing one on this page navigates to the
 * "ClassGroupsScreen" which shows groupings of upcoming wordpress events that are associated with
 * the selected event (aka class) category.
 */
export const ClassCategoriesScreen: FC<NavigationTabScreenProps> = props => {
  const {
    error: classCategoriesError,
    data: classCategoriesData = {},
    loading: classCategoriesLoading,
    refetch: refetchClassCategories,
  } = useQuery(CLASS_CATEGORIES_QUERY, {
    fetchPolicy: 'cache-and-network',
  });

  const navigateToClassGroupsScreen = (category: ClassCategory): void => {
    const eventCategoryParams = {
      eventCategoryId: category.id,
      eventCategoryName: category.name,
    };

    // ClassCategories Screen uses data provided from the json Wordpress api, while the
    // ClassGroupsScreen requests data directly from mysql. This is the reason for
    // passing the "classCategoryId" as the "eventCategoryId", etc. The name of the schema entity
    // changed from "class" to "event" as we migrated from the json Wordpress api to direct mysql.
    props.navigation.navigate('ClassGroups', eventCategoryParams);
  };

  const renderClassCategory = (category: ClassCategory): React.ReactNode => {
    const { id, name } = category;

    return (
      <Container key={id} pb={2} width="48%">
        <ClassCategoryCard
          label={name}
          onPress={(): void => navigateToClassGroupsScreen(category)}
          imageSrc={category.thumbnail}
        />
      </Container>
    );
  };

  const renderClassCategories = (): ReactElement => {
    const classCategories = classCategoriesData ? classCategoriesData.eventCategories : [];

    return (
      <Container flexDirection="row" flexWrap="wrap" centeredContent justifyContent="space-between">
        {classCategories.length ? classCategories.map(renderClassCategory) : null}
      </Container>
    );
  };

  const renderClassCategoriesScreenContent = (): ReactElement => {
    if (classCategoriesLoading && !Object.keys(classCategoriesData).length) {
      return (
        <Container fill pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          <Text testID="classes-screen-header" fontWeight="bold" fontSize={5} py={4}>
            {t('classes.categories.header')}
          </Text>
          <Container fill fullWidth centerContent>
            <ActivityIndicator />
          </Container>
        </Container>
      );
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        refreshControl={
          <RefreshControl refreshing={classCategoriesLoading} onRefresh={refetchClassCategories} />
        }
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          <Text testID="class-categories-screen-header" fontWeight="bold" fontSize={5} py={4}>
            {t('classes.categories.header')}
          </Text>
          <Divider style={{ backgroundColor: colors.silver }} />
          <Container flexDirection="row" justifyContent="flex-end" p={2} />
          {renderClassCategories()}
        </Container>
      </ScrollView>
    );
  };

  if (classCategoriesError) {
    // TODO: handle graphql errors
  }

  return (
    <Screen
      testID="class-categories-screen"
      screenHeader
      backgroundColor={colors.pearlLusta}
      fill
      height="100%"
      paddingTop={0}
    >
      {renderClassCategoriesScreenContent()}
    </Screen>
  );
};
