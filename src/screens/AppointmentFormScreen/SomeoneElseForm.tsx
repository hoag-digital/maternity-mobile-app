import React, { FC, useRef } from 'react';
import { Formik } from 'formik';
import { object, string } from 'yup';
import { Alert } from 'react-native';
import { TextInput } from '../../components/TextInput';
import { Button } from '../../components/Button';
import { Container } from '../../components/Container';
import { FormErrorSummary } from '../../components/FormErrorSummary';
import { FormFieldError } from '../../components/FormFieldError';
import { colors } from '../../styles';
import { t, allFieldsHaveValues } from '../../utils';
import { SharedFormElements } from './SharedFormElements';

const initialValues = {
  email: null,
  name: null,
  phone: null,
  dob: null,
  patientType: null,
  symptomDetails: null,
  date: null,
  time: null,
};

/**
 * Used on the AppointmentFormScreen
 */
export const SomeoneElseForm: FC = () => {
  const validationSchema = useRef(
    object().shape({
      email: string()
        .email(t('care.errorEmailRequired'))
        .required()
        .typeError(t('care.errorEmailRequired')),
      name: string()
        .required()
        .typeError(t('care.errorNameRequired')),
      phone: string()
        .required()
        .typeError(t('care.errorPhoneRequired')),
      dob: string()
        .required()
        .typeError(t('care.errorDOBRequired')),
    })
  );

  const onSubmit = (values, formikBag): void => {
    console.log(`---------------- SUBMITTED:  `, values, formikBag);
    Alert.alert('Submitted "Someone Else" form.');
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validateOnChange={false}
      validationSchema={validationSchema.current}
    >
      {({ handleChange, handleSubmit, values, errors, setFieldValue }): JSX.Element => (
        <Container>
          {Object.keys(errors).length ? (
            <FormErrorSummary>{t('care.errorAllFieldsRequired')}</FormErrorSummary>
          ) : null}
          <Container mb={1}>
            <TextInput
              placeholder={t('care.patientFirstAndLastName')}
              borderRadius={1}
              borderColor={colors.gainsboro}
              onChangeText={handleChange('name')}
              value={values.name}
            />
            {errors.name ? <FormFieldError px={4}>{errors.name}</FormFieldError> : null}
          </Container>
          <Container mb={1}>
            <TextInput
              placeholder={t('care.email')}
              borderRadius={1}
              borderColor={colors.gainsboro}
              onChangeText={handleChange('email')}
              value={values.email}
            />
            {errors.email ? <FormFieldError px={4}>{errors.email}</FormFieldError> : null}
          </Container>
          <Container mb={1}>
            <TextInput
              placeholder={t('care.cellPhoneNumber')}
              borderRadius={1}
              borderColor={colors.gainsboro}
              onChangeText={handleChange('phone')}
              value={values.phone}
            />
            {errors.phone ? <FormFieldError px={4}>{errors.phone}</FormFieldError> : null}
          </Container>
          <Container mb={1}>
            <TextInput
              placeholder={t('care.dateOfBirth')}
              borderRadius={1}
              borderColor={colors.gainsboro}
              onChangeText={handleChange('dob')}
              value={values.dob}
            />
            {errors.dob ? <FormFieldError px={4}>{errors.dob}</FormFieldError> : null}
          </Container>
          <SharedFormElements
            handleChange={handleChange}
            values={values}
            errors={errors}
            setFieldValue={setFieldValue}
          />
          <Container mx={0} alignItems="center">
            <Button
              disabled={!allFieldsHaveValues(Object.keys(initialValues), values)}
              backgroundColor={colors.malibu}
              width="100%"
              mx={1}
              mt={4}
              py={2}
              label={t('care.confirmAppointment')}
              color={colors.white}
              borderRadius={5}
              onPress={handleSubmit}
            />
          </Container>
        </Container>
      )}
    </Formik>
  );
};

export default SomeoneElseForm;
