import React, { FC, useRef } from 'react';
import { Alert } from 'react-native';
import { Formik } from 'formik';
import { object, mixed } from 'yup';
import { Button } from '../../components/Button';
import { Container } from '../../components/Container';
import { FormErrorSummary } from '../../components/FormErrorSummary';
import { colors } from '../../styles';
import { t, allFieldsHaveValues } from '../../utils';
import { SharedFormElements } from './SharedFormElements';

/**
 * Used on the AppointmentFormScreen
 */
export const ForMeForm: FC = () => {
  const validationSchema = useRef(
    object().shape({
      patientType: mixed().required(t('care.errorPatientTypeRequired')),
      symptomDetails: mixed().required(t('care.errorSymptomDetailsRequired')),
      date: mixed().required(t('care.errorDateRequired')),
      time: mixed().required(t('care.errorTimeRequired')),
    })
  );

  const initialValues = {
    patientType: null,
    symptomDetails: null,
    date: null,
    time: null,
  };

  const onSubmit = (values, formikBag): void => {
    console.log(`---------------- SUBMITTED:  `, values, formikBag);
    Alert.alert('Submitted "For Me" form.');
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validateOnChange={false}
      validationSchema={validationSchema.current}
    >
      {({ handleChange, handleSubmit, setFieldValue, values, errors }): JSX.Element => (
        <Container>
          {Object.keys(errors).length ? (
            <FormErrorSummary>{t('care.errorAllFieldsRequired')}</FormErrorSummary>
          ) : null}

          <SharedFormElements
            handleChange={handleChange}
            setFieldValue={setFieldValue}
            values={values}
            errors={errors}
          />

          <Container mx={0} alignItems="center">
            <Button
              disabled={!allFieldsHaveValues(Object.keys(initialValues), values)}
              backgroundColor={colors.malibu}
              width="100%"
              mx={1}
              mt={4}
              py={2}
              label={t('care.confirmAppointment')}
              color={colors.white}
              borderRadius={5}
              onPress={handleSubmit}
            />
          </Container>
        </Container>
      )}
    </Formik>
  );
};

export default ForMeForm;
