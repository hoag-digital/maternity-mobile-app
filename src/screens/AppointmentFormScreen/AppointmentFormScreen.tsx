import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';

import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';

import { colors } from '../../styles';

// const KEY_FOR_ME = 'for-me';
// const KEY_SOMEONE_ELSE = 'someone-else';

export const AppointmentFormScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const location = navigation.getParam('location');
  const appointmentUrl = `https://www.clockwisemd.com/hospitals/${location.id}/appointments/new`;
  // const [activeForm, setActiveForm] = React.useState('for-me');

  // TODO: add back when booking appointments locally
  // Two buttons that we display
  // Determines which form we show
  // const BUTTONS = [
  //   {
  //     label: t('care.forMe'),
  //     key: KEY_FOR_ME,
  //   },
  //   {
  //     label: t('care.someoneElse'),
  //     key: KEY_SOMEONE_ELSE,
  //   },
  // ];

  return (
    <Screen
      testID="care-screen"
      modalHeader
      screenTitle={location.name}
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      {/* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */}
      {/* <ScrollView scrollIndicatorInsets={{ right: 1 }}>
        <FastImage source={foothillsImage} style={{ height: 150 }} resizeMode="cover" />
        <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          <Container alignItems="center" flexDirection="row" justifyContent="center" mb={4}>
            {BUTTONS.map(button => (
              <Button
                key={button.key}
                backgroundColor={button.key === activeForm ? colors.balticSea : colors.whiteSmoke}
                width={130}
                height={30}
                mx={1}
                label={button.label}
                color={button.key === activeForm ? colors.white : colors.balticSea50}
                borderRadius={5}
                onPress={(): void => {
                  setActiveForm(button.key);
                }}
              />
            ))}
          </Container>
          {activeForm === KEY_SOMEONE_ELSE ? <SomeoneElseForm /> : null}
          {activeForm === KEY_FOR_ME ? (
            <>
              <LoggedInContent>
                <ForMeForm />
              </LoggedInContent>
              <LoggedOutContent>
                <SomeoneElseForm />
              </LoggedOutContent>
            </>
          ) : null}
        </Container>
      </ScrollView> */}

      <WebView source={{ uri: appointmentUrl }} containerStyle={StyleSheet.absoluteFillObject} />
    </Screen>
  );
};
