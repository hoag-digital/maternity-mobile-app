import AsyncStorage from '@react-native-community/async-storage';

export enum USER_ROLE {
  PREGNANT = 'pregnant',
  NEW_PARENT = 'new-parent',
}

const ONBOARDED = 'ONBOARDED';
const ESTIMATED_DUE_DATE = 'ESTIMATED_DUE_DATE';
const LAST_MENSTRUAL_PERIOD = 'LAST_MENSTRUAL_PERIOD';
const USER_STATUS = 'USER_STATUS';

let onboarded;
let estimatedDueDate;
let lastMenstrualPeriod;
let userStatus; //will either be pregnant or new-parent;

//#region Estimated Due Date Settings
export const getEstimatedDueDate = async (): Promise<string> => {
  if (estimatedDueDate) {
    return Promise.resolve(estimatedDueDate);
  }

  estimatedDueDate = await AsyncStorage.getItem(ESTIMATED_DUE_DATE);

  return estimatedDueDate;
};

export const setEstimatedDueDate = (estDueDate: string): Promise<void> => {
  return AsyncStorage.setItem(ESTIMATED_DUE_DATE, estDueDate);
};

export const clearEstimatedDueDate = (): Promise<void> => {
  estimatedDueDate = undefined;

  return AsyncStorage.removeItem(ESTIMATED_DUE_DATE);
};
//#endregion

//#region Last Menstrual Period Settings
export const getLastMenstrualPeriod = async (): Promise<string> => {
  if (lastMenstrualPeriod) {
    return Promise.resolve(lastMenstrualPeriod);
  }

  lastMenstrualPeriod = await AsyncStorage.getItem(LAST_MENSTRUAL_PERIOD);

  return lastMenstrualPeriod;
};

export const setLastMenstrualPeriod = (lastMenstrualDt: string): Promise<void> => {
  return AsyncStorage.setItem(LAST_MENSTRUAL_PERIOD, lastMenstrualDt);
};

export const clearLastMenstrualPeriod = (): Promise<void> => {
  lastMenstrualPeriod = undefined;

  return AsyncStorage.removeItem(LAST_MENSTRUAL_PERIOD);
};
//#endregion

//#region User Status (pregnant or new-parent)
export const getUserStatus = async (): Promise<USER_ROLE> => {
  if (userStatus) {
    return Promise.resolve(userStatus);
  }

  userStatus = await AsyncStorage.getItem(userStatus);

  return userStatus;
};

export const setUserStatus = (status: USER_ROLE): Promise<void> => {
  return AsyncStorage.setItem(USER_STATUS, status);
};

export const clearUserStatus = (): Promise<void> => {
  userStatus = undefined;

  return AsyncStorage.removeItem(USER_STATUS);
};
//#endregion

//#region Onboarded Completed
export const getOnboarded = async (): Promise<boolean> => {
  if (onboarded) {
    return Promise.resolve(true);
  }

  onboarded = await AsyncStorage.getItem(ONBOARDED);
  if (onboarded === null) return false;

  return onboarded;
};

export const setOnboarded = (): Promise<void> => {
  return AsyncStorage.setItem(ONBOARDED, 'true');
};

export const clearOnboarded = (): Promise<void> => {
  onboarded = undefined;

  return AsyncStorage.removeItem(ONBOARDED);
};
//#endregion
