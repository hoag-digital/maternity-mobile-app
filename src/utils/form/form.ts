/**
 * Determine whether all fields in a Formik form are non-empty.
 * @param requiredFields Array of field names.
 * @param values A values object from inside a Formik form render.
 */
export const allFieldsHaveValues = (
  requiredFields: string[],
  values: Record<string, any>
): boolean => {
  return requiredFields.every(key => !!values[key]);
};
