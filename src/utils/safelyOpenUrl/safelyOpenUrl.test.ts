import safelyOpenUrl from './safelyOpenUrl'

describe('no arguments', () => {
  const GOOD_URL = 'https://www.google.com';
  const BAD_URL = 'askjhdkjasghdkj';

  test('returns null for no input', async () => {
    const result = await safelyOpenUrl();

    expect(result).toBe(null);
  });

  test('should not throw an error when a bad url is input', () => {
    expect(() => safelyOpenUrl(BAD_URL)).not.toThrowError();
  });

  test('should not throw an error when a good url is input', () => {
    expect(() => safelyOpenUrl(GOOD_URL)).not.toThrowError();
  });
})
