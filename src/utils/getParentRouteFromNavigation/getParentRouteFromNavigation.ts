import { NavigationTabProp } from 'react-navigation-tabs';

/**
 * Function for retrieving the parent navigation route from the navigation prop.
 * @param navigation - the navigation screen prop
 */
const getParentRouteFromNavigation = (navigation: NavigationTabProp): string | null => {
  if (!navigation) return null;

  // Method for accessing the parent navigation state:
  // https://reactnavigation.org/docs/4.x/navigation-prop#dangerouslygetparent---get-parent-navigator
  // from my experience this will be reliable, but it looks like the method was prepended with
  // "dangerously" so the maintainers of react-navigation could potentially refactor if they needed to.
  // I don't see this being an issue as long as we stay on react-navigation v4.
  const parent = navigation.dangerouslyGetParent();

  const parentRoute = parent && parent.state && parent.state.routeName;

  // only return the result if "routeName" was reached
  return typeof parentRoute === 'string' ? parentRoute : null;
};

export default getParentRouteFromNavigation;
