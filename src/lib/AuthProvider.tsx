import React, { Children, ReactElement } from 'react';
import PropTypes from 'prop-types';
import AuthContext from './AuthContext';
import { AuthPropType } from './AuthUtils';

interface Props {
  auth?: AuthPropType;
}

/**
 * Provider for authentication context.
 */
const AuthProvider = (props): ReactElement => {
  return (
    <AuthContext.Provider value={props.auth}>{Children.only(props.children)}</AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.element.isRequired,
  auth: PropTypes.instanceOf(Object).isRequired,
};

export default AuthProvider;
