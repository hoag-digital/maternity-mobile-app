import AuthContext from './AuthContext';

/**
 * Provided for syntactic consistency with AuthContext and AuthProvider.
 */
export const AuthConsumer = AuthContext.Consumer;

export default AuthConsumer;
