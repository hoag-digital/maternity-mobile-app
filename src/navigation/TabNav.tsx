import React, { ReactElement } from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { colors } from '../styles';
import { DEFAULT_ICON_SIZE, TAB_BAR_HEIGHT } from '../utils/constants';

import { space } from '../styles/margins';

import HomeIcon from '../assets/images/icons/home.svg';
import HomeActiveIcon from '../assets/images/icons/home-active.svg';
import ClassIcon from '../assets/images/icons/classes.svg';
import ClassActiveIcon from '../assets/images/icons/classes-active.svg';
import MomIcon from '../assets/images/icons/mom.svg';
import MomActiveIcon from '../assets/images/icons/mom-active.svg';
import MoreIcon from '../assets/images/icons/more.svg';
import MoreActiveIcon from '../assets/images/icons/more-active.svg';
import ScheduleIcon from '../assets/images/icons/schedule.svg';
import ScheduleActiveIcon from '../assets/images/icons/schedule-active.svg';
import { MoreScreen } from '../screens/MoreScreen';
import { HomeStackNav } from './HomeStackNav';
import { ClassStackNav } from './ClassStackNav';
import { MeStackNav } from './MeStackNav';
import { ScheduleStackNav } from './ScheduleStackNav';

/**
 * Main Nav is main interface of the app, defaults to tabs.
 */
export const TabNav = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStackNav,
      navigationOptions: {
        tabBarTestID: 'home-screen',
        tabBarIcon: (props): ReactElement =>
          props.focused ? (
            <HomeActiveIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ) : (
            <HomeIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ),
      },
    },
    ClassCategories: {
      screen: ClassStackNav,
      navigationOptions: {
        tabBarTestID: 'class-categories-screen',
        tabBarIcon: (props): ReactElement =>
          props.focused ? (
            <ClassActiveIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ) : (
            <ClassIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ),
        tabBarLabel: 'Classes',
      },
    },
    Me: {
      screen: MeStackNav,
      navigationOptions: {
        tabBarTestID: 'me-screen',
        tabBarIcon: (props): ReactElement =>
          props.focused ? (
            <MomActiveIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ) : (
            <MomIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ),
        tabBarLabel: 'Me',
      },
    },
    Schedule: {
      screen: ScheduleStackNav,
      navigationOptions: {
        tabBarTestID: 'my-schedule-screen',
        tabBarIcon: (props): ReactElement => {
          return props.focused ? (
            <ScheduleActiveIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ) : (
            <ScheduleIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          );
        },
      },
    },
    More: {
      screen: MoreScreen,
      navigationOptions: {
        tabBarTestID: 'more-screen',
        tabBarIcon: (props): ReactElement => {
          return props.focused ? (
            <MoreActiveIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          ) : (
            <MoreIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
          );
        },
      },
    },
  },
  {
    initialRouteName: 'Home',
    lazy: true,
    tabBarOptions: {
      activeTintColor: colors.black,
      inactiveTintColor: colors.balticSea50,
      showLabel: true,
      style: {
        backgroundColor: colors.whiteTransparent,
        position: 'absolute', // must have positioning or it will force the screen elements to stop above the tabs versus go behind them
        left: 0,
        bottom: 0,
        right: 0,
        paddingTop: space[2],
        paddingBottom: space[2],
        height: TAB_BAR_HEIGHT,
      },
    },
  }
);
