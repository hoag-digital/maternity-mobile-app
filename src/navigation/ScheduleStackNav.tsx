import { createStackNavigator } from 'react-navigation-stack';
import { ScheduleScreen } from '../screens/ScheduleScreen';
import { ClassDetailsScreen } from '../screens/ClassDetailsScreen';
import { ProgramDetailsScreen } from '../screens/ProgramDetailsScreen';

/**
 * ScheduleStackNav is the nav stack available from the Schedule tab of the TabNav.
 * Note that a "Class" is a generic wordpress event.
 * Note that a "Program" is a wordpress event requiring an Rx token.
 */
export const ScheduleStackNav = createStackNavigator(
  {
    Schedule: {
      screen: ScheduleScreen,
    },
    ClassDetails: {
      screen: ClassDetailsScreen,
    },
    ProgramDetails: {
      screen: ProgramDetailsScreen,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Schedule',
  }
);
