import { createAppContainer, createSwitchNavigator, NavigationContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { ReservationConfirmationScreen } from '../screens/ReservationConfirmationScreen';
import { DrawerNav } from './DrawerNav';
import { GuestNav } from './GuestNav';
import { WelcomeStackNav } from './WelcomeStackNav';

/**
 * AppNav is the primary stack visible to a logged-in user.
 */

const AppNav = createStackNavigator(
  {
    Home: {
      screen: DrawerNav,
      navigationOptions: {
        // header: () => <ScreenHeader drawerEnabled />,
      },
    },
    // If you need a top level modal, for logged in users, put it here
    ReservationConfirmation: {
      screen: ReservationConfirmationScreen,
    },
    Welcome: {
      screen: WelcomeStackNav,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Welcome',
    mode: 'modal',
  }
);

/**
 * Creates a top level Switch Navigator, using currentUser to set the
 * initial route (App or Guest Nav)
 */
export interface CreateRootNavParams {
  /** Current user of the app. If truthy, renders AppNav. Otherwise, GuestNav. Typically an object, but booleans and strings will also work. */
  currentUser?: Record<string, string> | boolean | string;
}

export const createRootNav = ({}: CreateRootNavParams): NavigationContainer => {
  const rootNav = createSwitchNavigator(
    {
      AppNav,
      GuestNav,
    },
    {
      initialRouteName: 'AppNav', // currentUser ? 'AppNav' : 'GuestNav',
    }
  );

  return createAppContainer(rootNav);
};
