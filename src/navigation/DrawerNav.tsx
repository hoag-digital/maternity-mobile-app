import React from 'react';
import { createDrawerNavigator } from 'react-navigation-drawer';

import { colors } from '../styles';
import { fontSizes } from '../styles/fonts';
import { space } from '../styles/margins';
import { CustomDrawerNav, onItemPress } from '../components/CustomDrawerNav';
import { AuthConsumer } from '../lib/AuthConsumer';
import { MENU_WIDTH } from '../utils';
import { TabNav } from './TabNav';

const DRAWER_WIDTH = MENU_WIDTH + space[4];

const renderEmptyScreen = (): null => null;

const emptyScreenRouteConfig = {
  // No screen here; behavior is handled by the custom onItemPress below, provided by CustomDrawerNav.
  screen: renderEmptyScreen,
};

// These are copies of the TabNav routes with `-Tab` appended
const MOCK_TAB_ROUTES = {
  ['Home-Tab']: emptyScreenRouteConfig,
  ['ClassCategories-Tab']: emptyScreenRouteConfig,
  ['Me-Tab']: emptyScreenRouteConfig,
  ['Schedule-Tab']: emptyScreenRouteConfig,
};

/**
 * DrawerNav contains the additional screens contained within the "More" tab
 * of the TabNav tab navigator.
 */
export const DrawerNav = createDrawerNavigator(
  {
    Home: {
      screen: TabNav,
    },
    ...MOCK_TAB_ROUTES,
    Logout: emptyScreenRouteConfig,
  },
  {
    drawerWidth: DRAWER_WIDTH,
    drawerPosition: 'left',
    drawerBackgroundColor: colors.snow,
    overlayColor: colors.grayTransparent,
    drawerType: 'front',
    lazy: true, // this is in the docs but missing in the types; https://reactnavigation.org/docs/en/drawer-navigator.html
    contentComponent: drawerNavProps => {
      return (
        <AuthConsumer>
          {(auth): React.ReactNode => {
            const isLoggedOut = !auth || !auth.token;

            return (
              <CustomDrawerNav
                {...drawerNavProps} // make sure all the regular stuff gets passed down a self-defined CustomDrawer
                isLoggedOut={isLoggedOut}
                onItemPress={(itemPressProps): Promise<any> =>
                  onItemPress(drawerNavProps, itemPressProps, auth)
                }
              />
            );
          }}
        </AuthConsumer>
      );
    },
    contentOptions: {
      labelStyle: {
        paddingHorizontal: space[1],
        paddingVertical: space[1],
        fontSize: fontSizes[3],
        color: colors.chino,
      },
      itemStyle: {
        backgroundColor: colors.transparent,
      },
    },
  }
);
