import { createStackNavigator } from 'react-navigation-stack';
import { CareScreen } from '../screens/CareScreen';
import { AppointmentFormScreen } from '../screens/AppointmentFormScreen/AppointmentFormScreen';
import { ScheduleFoothillAppointmentScreen } from '../screens/ScheduleFoothillAppointmentScreen';
import { WomensScreen } from '../screens/WomensScreen';
import { TelehealthScreen } from '../screens/TelehealthScreen';
import { SelfAssessmentScreen } from '../screens/SelfAssessmentScreen';

/**
 * CareNav: used for navigation pertaining to the "Care" tab
 */
const CareNav = createStackNavigator(
  {
    Default: {
      screen: CareScreen,
      navigationOptions: {
        header: null,
      },
    },
    'Appointment Form': {
      screen: AppointmentFormScreen,
      navigationOptions: {
        header: null,
      },
    },
    'Schedule Foothill Appointment': {
      screen: ScheduleFoothillAppointmentScreen,
      navigationOptions: {
        header: null,
      },
    },
    Womens: {
      screen: WomensScreen,
      navigationOptions: {
        header: null,
      },
    },
    Telehealth: {
      screen: TelehealthScreen,
      navigationOptions: {
        header: null,
      },
    },
    'Self Assessment': {
      screen: SelfAssessmentScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Default',
  }
);

/**
 * This code snippet lets us hide the TabBar on specific nested StackNavigation screens.
 * See: https://github.com/react-navigation/react-navigation/issues/5790#issuecomment-517104205
 */
CareNav.navigationOptions = ({ navigation }): any => {
  let tabBarVisible = true;

  for (let i = 0; i < navigation.state.routes.length; i++) {
    if (navigation.state.routes[i].routeName === 'Self Assessment') {
      tabBarVisible = false;
    }
  }

  return {
    tabBarVisible,
  };
};

export { CareNav };
