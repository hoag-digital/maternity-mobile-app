import React from 'react';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';
import { OnboardingScreen1 } from '../screens/Onboarding/OnboardingScreen1';
import { CustomizingExperienceScreen } from '../screens/Onboarding/CustomizingExperienceScreen';
import { WelcomeScreen } from '../screens/WelcomeScreen';
import { OnboardingScreen2 } from '../screens/Onboarding/OnboardingScreen2';
import { OnboardingScreen3 } from '../screens/Onboarding/OnboardingScreen3';

/**
 * WelcomeStackNav is the StackNavigator available when first launching the applicaiton and going through
 * onboarding if applicable
 */

export const WelcomeStackNav = createAnimatedSwitchNavigator(
  {
    Welcome: {
      screen: WelcomeScreen,
    },
    Onboarding1: {
      screen: OnboardingScreen1,
    },
    Onboarding2: {
      screen: OnboardingScreen2,
    },
    Onboarding3: {
      screen: OnboardingScreen3,
    },
    CustomizeExperience: {
      screen: CustomizingExperienceScreen,
    },
  },
  {
    initialRouteName: 'Welcome',
    transition: (
      <Transition.Together>
        <Transition.Out type="fade" durationMs={800} interpolation="easeOut" />
        <Transition.In type="fade" durationMs={600} delayMs={200} interpolation="easeIn" />
      </Transition.Together>
    ),
  }
);
