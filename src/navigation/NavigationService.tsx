/**
 * Utility for navigating without direct access to the navigation prop
 * From: https://reactnavigation.org/docs/4.x/navigating-without-navigation-prop
 */
import { NavigationActions } from 'react-navigation';

let navigator;

const setTopLevelNavigator = (navigatorRef: any): void => {
  navigator = navigatorRef;
};

const navigate = (routeName: string, params: any, action?: any): void => {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
      action,
    })
  );
};

export default {
  navigate,
  setTopLevelNavigator,
};
