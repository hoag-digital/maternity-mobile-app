import { createStackNavigator } from 'react-navigation-stack';
import { MeIndexScreen } from '../screens/MeIndexScreen';

/**
 * Guest nav typically consists of screens where the user is logged out:
 *   Onboarding
 *   Registration
 *   Login
 *   Forgot Password
 */
export const MeStackNav = createStackNavigator(
  {
    MeIndex: {
      screen: MeIndexScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'MeIndex',
  }
);
