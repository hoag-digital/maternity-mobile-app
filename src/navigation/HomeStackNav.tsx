import { createStackNavigator } from 'react-navigation-stack';

import { HomeScreen } from '../screens/HomeScreen';
import { PostDetailsScreen } from '../screens/PostDetailsScreen';
import { ClassDetailsScreen } from '../screens/ClassDetailsScreen';

/**
 * HomeStackNav is the nav stack available from the Home tab of the TabNav
 */
export const HomeStackNav = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    PostDetails: {
      screen: PostDetailsScreen,
    },
    ClassDetails: {
      screen: ClassDetailsScreen,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
  }
);
