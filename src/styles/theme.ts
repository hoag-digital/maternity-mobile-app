import { colors } from './colors';
import { fonts as fontFamilies, fontSizes, lineHeights, weights } from './fonts';
import { margins, space, radii, radius } from './margins';

export const shadows = {
  small: `5px 5px 7px ${colors.silver}`,
  large: `10px 10px 10px ${colors.silver}`,
};

export const theme = {
  colors,
  fontFamilies,
  fontSizes,
  lineHeights,
  margins,
  radii,
  radius,
  shadows,
  space,
  weights,
};

export default theme;
