export default {
  welcome: 'Bienvenido, inicia sesión.',
  login: 'Iniciar sesión',
  password: 'Contraseña',
  email: 'correo electrónico',
  buttons: {
    login: 'Iniciar sesión',
    cancel: 'cancelar',
    forgotPassword: '¿Se te olvidó tu contraseña?',
    createAccount: 'Crear una cuenta',
  },
  error: 'Error al iniciar sesión, intente nuevamente',
};
