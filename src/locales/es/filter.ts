export default {
  label: 'Filtrar',
  all: 'Todos',
  classes: 'Clases',
  news: 'Noticias',
  media: 'Medios de comunicación',
  videos: 'Videos',
};
