import filter from './filter';
import home from './home';
import login from './login';
import navigation from './navigation';
import registration from './registration';
import programs from './programs';

export default {
  filter,
  home,
  login,
  navigation,
  programs,
  registration,
};
