export default {
  buttons: {
    next: 'Next',
    back: 'Back',
  },
  firstStep: {
    title: 'Are you...',
    pregnantOption: 'already pregnant',
    newParentOption: 'a new parent',
  },
  secondStep: {
    title: 'How far along\nare you?',
    lastMenstrual: 'Last menstrual period',
    or: 'or',
    estimatedDueDate: 'Estimated due date',
  },
  thirdStep: {
    title: 'Pre-Admission\nRegistration',
    subtitle: 'Would you like to sign up\nfor the pre-admission\nregistration form?',
    buttons: {
      no: 'No',
      signup: 'Sign up',
    },
  },
  customization: {
    line1: `Customizing\nyour experience.`,
    line2: 'Enjoy the app!',
  },
};
