export default {
  header: 'Schedule',
  noScheduledItemsCTA: 'Browse Categories',
  noScheduledItems: 'You do not have anything scheduled.',
  defaultClassTitle: 'HoagClass',
  cancelClass: 'Cancel Class?',
  authProtectedMessage: 'Please login to view your class schedule.',
  loginCTA: 'Login',
};
