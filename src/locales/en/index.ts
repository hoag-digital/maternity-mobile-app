import care from './care';
import classes from './classes';
import errors from './errors';
import filter from './filter';
import home from './home';
import login from './login';
import navigation from './navigation';
import programs from './programs';
import registration from './registration';
import schedule from './schedule';
import welcome from './welcome';

//TODO:CLEAN UP the unused references from HealthyTogether

import onboarding from './onboarding';

export default {
  care,
  classes,
  errors,
  filter,
  home,
  login,
  navigation,
  programs,
  registration,
  schedule,
  welcome,
  onboarding,
};
