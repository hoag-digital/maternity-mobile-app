export default {
  welcome: 'Welcome, please sign in.',
  login: 'Login',
  password: 'Password',
  email: 'Email',
  buttons: {
    login: 'Login',
    cancel: 'Cancel',
    forgotPassword: 'Forgot Password?',
    createAccount: 'Create Account',
  },
  error: 'Error Logging In, please try again',
};
