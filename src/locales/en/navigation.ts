export default {
  home: 'Home',
  care: 'Care',
  me: 'Me',
  classes: 'Classes',
  programs: 'Programs',
  schedule: 'Schedule',
};
