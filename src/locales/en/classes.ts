export default {
  categories: {
    header: 'Live Stream Classes',
  },
  waiver: {
    header: 'Waiver',
    clear: 'Clear',
    agree: 'Agree',
    date: 'Date',
  },
  introduction: {
    skip: 'Skip',
  },
  confirmation: {
    title: 'Your class has been scheduled.',
    subtitle: 'Thank you!',
    primaryActionLabel: 'My Schedule',
    secondaryActionLabel: 'Back to my classes',
  },
  title: 'Class',
};
