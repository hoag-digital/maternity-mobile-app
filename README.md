# Welcome to Hoag Hoag Maternity Mobile App 👋

> A React Native mobile app for the Hoag Womens Health Clinic, for women that are expecting or post-partum 🤰🏽 👶

## Table of Contents

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Starting the Project](#starting-the-project)
    - [iOS](#ios)
    - [Android](#android)
  - [Running Tests](#running-tests)
  - [Running Storybook](#running-storybook)
  - [Environment Variables](#environment-variables)

## Getting Started

In order to get started with this project, you'll need to get your local environment set up. Please read each section below.

### Prerequisites

This is a React Native project. If you haven't already, please follow the ["Getting Started"](https://facebook.github.io/react-native/docs/getting-started) section in the React Native docs. Use the "React Native CLI Quickstart" section. If you are asked to download the `react-native` CLI globally, use `npx` instead.

Make sure you follow the guide to set up for both iOS and Android.

### Starting the Project

To start the project, clone the repo then, from the root, run:

```sh
yarn
```

This will install the dependencies.

Check for any required system dependencies with `npx solidarity`, which will let you know if you need to install any binaries outside of the project root.

#### iOS

To actually start the project, run:

```sh
yarn ios
```

This will start Metro bundler, the iOS simulator and run the app on the iPhone X.

#### Android

The process for Android is a bit similar. First, you'll run:

```sh
yarn start
```

This will start Metro bundler. Then run:

```sh
yarn emulator:start
```

This will start the Android device emulator and run the app on a Pixel 2. You'll need to have the `Pixel_2_API_29` installed to run the app. To do this, follow these steps:

1. Open Android Studio
2. Click "Configure" > AVD Manager
3. Click "Create Virtual Device"
4. Select "Pixel 2" then select "Next"
   ![Select Pixel 2 screenshot.](https://i.ibb.co/ypHrMZv/select-pixel-2.png)
5. Select "Q" (API Level 29) then select "Next"
   ![Select Q system.](https://i.ibb.co/ZfMHGm4/select-q-system.png)
6. Make sure AVD Name is "Pixel 2 API 29"
   ![Check AVD Name.](https://i.ibb.co/p0q3Bpp/avd-name.png)
7. Click "Finish"

*NOTE* you can create other emulators but this is the default used right now.

### Running Tests

To run tests, run:

```sh
yarn test
```

### Running Storybook

Storybook is used for component development and can be run by setting the STORYBOOK_MODE=true in the `.env` file. When updating `.env` you need to reset the bundle cache by running the following command:

```sh
yarn start
```

### Environment Variables

Default environment configuration is `.env` file and you can additionally use `.env.production` in release configuration. By default dotenv treats all values as string, we utilize [dotenv-parse-variables](https://www.npmjs.com/package/dotenv-parse-variables) to convert variable typings.

Use the ENV variables by importing `env.ts` file as shown in this example. Env vars are stored in 1Password Hoag Vault during development but can also be pulled down from the [context repo](https://bitbucket.org/hoagdev/maternitymobile-context/src/) using Fastlane. `Fastlane run decrypt_app_vars` from the root of the project. You will need the cryptex passphrase and git url.

```ts
import ENV from './env';

const { STORYBOOK_MODE } = ENV;
```

### Localization

Localization support is done with [react-native-localize](https://github.com/react-native-community/react-native-localize) and [i18n-js](https://github.com/fnando/i18n-js)

Files currently available for English and Spanish, found under `src/locales`

- en/es folders maintain the speparation for locale but key/value pairs and structure MUST match, with values holding the localized version
  - create a `.ts` to hold a namespaced area of key/value pairs
  - Add a new translation key/value pair in each file
- To retrieve localalized value import the translate function and provide the key

```ts
import { translate } from '../../utils';

-- or --

import { t } from '../../utils'; //shorthand for translate to save keystrokes
...

<Text> {translate('home.welcome')} </Text>

<Text> {t('home.welcome')}</Text>

...
```

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
