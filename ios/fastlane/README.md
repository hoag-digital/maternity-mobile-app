fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios alpha
```
fastlane ios alpha
```
Alpha build
### ios beta
```
fastlane ios beta
```
Beta build
### ios beta_local
```
fastlane ios beta_local
```
This runs tasks typically done on CI before calling the beta lane
### ios release
```
fastlane ios release
```
Release build
### ios release_local
```
fastlane ios release_local
```
This runs tasks typically done on CI before calling the release lane

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
