version: 2.1

orbs:
  react-native: echobind/react-native@0.3.0
  react-native-community: react-native-community/react-native@1.2.1

executors:
  node:
    docker:
      - image: circleci/node:10.16.0-stretch
        environment:
          TERM: xterm
  android:
    docker:
      - image: circleci/android:api-28-node
    resource_class: large # default is medium
    environment:
      - _JAVA_OPTIONS: '-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap'
      - GRADLE_OPTS: '-Dorg.gradle.daemon=false -Dorg.gradle.jvmargs="-XX:+UnlockExperimentalVMOptions -XX:+HeapDumpOnOutOfMemoryError"'
      - BUILD_THREADS: 2
      - FL_OUTPUT_DIR: output
  ios:
    macos:
      xcode: 11.3.1
    shell: /bin/bash --login -o pipefail
    resource_class: 'medium'

jobs:
  #################
  # NODE / Jest
  #################
  install_deps_and_run_jest:
    working_directory: ~/react-native
    executor: node
    steps:
      - checkout
      - run:
          name: 'Install libjpeg'
          command: 'sudo apt-get install libjpeg62'
      - react-native/jest:
          cache_key: v1

  #################
  # Android
  #################
  android:
    working_directory: ~/react-native
    executor: android
    parameters:
      build:
        description: The build command to run. This is typically a fastlane lane.
        type: steps
        default: []
    steps:
      - checkout
      - attach_workspace:
          at: ~/react-native
      - run:
          name: set Ruby version
          command: echo 'chruby ruby-2.6' >> ~/.bash_profile
      - react-native/bundle_install_with_cache:
          cache_key: android-v1
          working_directory: android
      - react-native/download_gradle_dependencies_with_cache:
          cache_key: android-jars-v1
      - steps: << parameters.build >>
      - store_artifacts:
          path: android/app/build/outputs/bundle/release

  ios:
    working_directory: ~/react-native
    executor: ios
    parameters:
      build:
        description: The build command to run. This is typically a fastlane lane.
        type: steps
        default: []
    steps:
      - checkout
      - attach_workspace:
          at: ~/react-native
      - run:
          name: set Ruby version
          command: echo 'chruby ruby-2.6' >> ~/.bash_profile
      - restore_cache:
          key: 'brew-v1'
      - run:
          name: 'Install Homebrew Packages'
          command: brew update && brew install watchman && brew install libjpeg
      - save_cache:
          key: 'brew-v1'
          paths:
            - /usr/local/Homebrew
      - run: gem install cocoapods-check
      - run:
          name: 'Install Bundler version 2.1.4'
          command: gem install bundler
      - react-native/bundle_install_with_cache:
          cache_key: ios-v1
          working_directory: ios
      - restore_cache:
          key: 2-pods-{{ checksum "ios/Podfile.lock" }}
      - run:
          name: Install CocoaPods
          working_directory: ios
          command: |
            pod check ||
            (curl https://cocoapods-specs.circleci.com/fetch-cocoapods-repo-from-s3.sh | bash -s cf &&
            pod install)
      - save_cache:
          key: 2-pods-{{ checksum "ios/Podfile.lock" }}
          paths:
            - Pods
      - steps: << parameters.build >>
      - run:
          name: Copy outputs for artifacts
          command: |
            mkdir -p /tmp/artifacts
            cp ios/*.ipa /tmp/artifacts/
            cp ios/*.dSYM* /tmp/artifacts/
      - store_artifacts:
          path: /tmp/artifacts

workflows:
  version: 2

  ##############################
  # MAIN WORKFLOW - Run on every push
  ##############################
  main:
    jobs:
      - install_deps_and_run_jest

      ##############################
      # React Native Android
      ##############################
      - android:
          name: android
          filters:
            branches:
              only:
                - master
                - release
                - beta
          requires:
            - install_deps_and_run_jest
          build:
            - run:
                name: Set Git Commit Email
                command: git config --global user.email "$GIT_AUTHOR_EMAIL"
            - run:
                name: Set Git Commit Name
                command: git config --global user.name "$GIT_AUTHOR_NAME"
            - run:
                name: fastlane android
                working_directory: android
                command: |
                  if [[ "${CIRCLE_BRANCH}" =~ ^beta ]]; then
                    bundle exec fastlane beta build_number:${CIRCLE_BUILD_NUM}
                  elif [[ "${CIRCLE_BRANCH}" =~ ^release ]]; then
                    bundle exec fastlane release build_number:${CIRCLE_BUILD_NUM}
                  else
                    bundle exec fastlane alpha distribute:false build_number:${CIRCLE_BUILD_NUM}
                  fi

      ##############################
      # React Native iOS
      ##############################
      - ios:
          name: ios
          filters:
            branches:
              only:
                - master
                - release
                - beta
          requires:
            - install_deps_and_run_jest
          build:
            - run:
                name: Set Git Commit Email
                command: git config --global user.email "$GIT_AUTHOR_EMAIL"
            - run:
                name: Set Git Commit Name
                command: git config --global user.name "$GIT_AUTHOR_NAME"
            - run:
                name: fastlane ios
                working_directory: ios
                command: |
                  if [[ "${CIRCLE_BRANCH}" =~ ^beta ]]; then
                    bundle exec fastlane beta build_number:${CIRCLE_BUILD_NUM}
                  elif [[ "${CIRCLE_BRANCH}" =~ ^release ]]; then
                    bundle exec fastlane release build_number:${CIRCLE_BUILD_NUM}
                  else
                    bundle exec fastlane alpha distribute:false build_number:${CIRCLE_BUILD_NUM}
                  fi
