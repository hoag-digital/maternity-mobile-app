describe('Hoag Maternity', () => {
  beforeEach(() => {
    device.reloadReactNative();
  });

  it('should have home screen', () => {
    expect(element(by.id('homeScreen'))).toBeVisible();
  });

  describe('Home Screen', () => {
    it('should have header with drawer', () => {
      expect(element(by.id('drawer-icon'))).toBeVisible();
    });

    it('should have urgent care card', () => {
      expect(element(by.id('urgent-care'))).toBeVisible();
    });

    it('should have welcome text', () => {
      expect(element(by.id('welcome-text'))).toBeVisible();
      expect(element(by.id('welcome-text'))).toHaveText('Welcome to Foothill Ranch.');
    });
  });
});
